CREATE DATABASE  IF NOT EXISTS `impuestos` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `impuestos`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: impuestos
-- ------------------------------------------------------
-- Server version	5.7.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fecha_descuento`
--

DROP TABLE IF EXISTS `fecha_descuento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha_descuento` (
  `ID_FECHA_DESCUENTO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FECHA_DESCUENTO` date NOT NULL,
  PRIMARY KEY (`ID_FECHA_DESCUENTO`),
  UNIQUE KEY `ID_FECHA_DESCUENTO_UNIQUE` (`ID_FECHA_DESCUENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha_descuento`
--

LOCK TABLES `fecha_descuento` WRITE;
/*!40000 ALTER TABLE `fecha_descuento` DISABLE KEYS */;
INSERT INTO `fecha_descuento` VALUES (1,'2016-05-01'),(2,'2017-05-01'),(3,'2018-05-01');
/*!40000 ALTER TABLE `fecha_descuento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fecha_pago`
--

DROP TABLE IF EXISTS `fecha_pago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fecha_pago` (
  `ID_FECHA_PAGO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FECHA_PAGO` date NOT NULL,
  PRIMARY KEY (`ID_FECHA_PAGO`),
  UNIQUE KEY `ID_FECHA_PAGO_UNIQUE` (`ID_FECHA_PAGO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fecha_pago`
--

LOCK TABLES `fecha_pago` WRITE;
/*!40000 ALTER TABLE `fecha_pago` DISABLE KEYS */;
INSERT INTO `fecha_pago` VALUES (1,'2016-07-01'),(2,'2017-07-01'),(3,'2018-07-01');
/*!40000 ALTER TABLE `fecha_pago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linea`
--

DROP TABLE IF EXISTS `linea`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linea` (
  `ID_LINEA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `LINEA` varchar(45) NOT NULL,
  `ID_MARCA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_LINEA`),
  KEY `FK_ID_MARCA_idx` (`ID_MARCA`),
  CONSTRAINT `FK_ID_MARCA` FOREIGN KEY (`ID_MARCA`) REFERENCES `marca` (`ID_MARCA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linea`
--

LOCK TABLES `linea` WRITE;
/*!40000 ALTER TABLE `linea` DISABLE KEYS */;
INSERT INTO `linea` VALUES (1,'Spark',2),(2,'Sail',2),(3,'Onix',2),(4,'Sonic',2),(5,'Cruze',2),(6,'Camaro',2),(7,'Sandero',8),(8,'Sandero Stepway',8),(9,'Duster',8),(10,'CAPTUR',8),(11,'Koleos',8),(12,'Twizy',8),(13,'A1',119),(14,'A3',119),(15,'A4',119),(16,'A5',119),(17,'A6',119),(18,'A7',119),(19,'A8',119),(20,'Q2',119),(21,'Q3',119),(22,'Q5',119),(23,'Q7',119),(24,'TT',119),(25,'R8',119);
/*!40000 ALTER TABLE `linea` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `ID_MARCA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MARCA` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_MARCA`),
  UNIQUE KEY `ID_MARCA_UNIQUE` (`ID_MARCA`),
  UNIQUE KEY `MARCA_UNIQUE` (`MARCA`)
) ENGINE=InnoDB AUTO_INCREMENT=675 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (580,'2008 Cnenlon'),(310,'2140'),(118,'Abarth'),(652,'Acb'),(470,'Accura'),(584,'Accuro'),(573,'Ace'),(583,'Acm-Zhongyu'),(277,'Acura Legend'),(545,'Aeolus'),(253,'Agrale'),(653,'Akita'),(315,'Akt'),(278,'Aleko'),(26,'Alfa Romeo'),(263,'Allad'),(537,'Allis Chalmers'),(279,'Ambar'),(350,'Amc'),(88,'American Motor'),(658,'Amw'),(518,'Anglia'),(54,'Ap Piaggio'),(211,'Ape Piaggio'),(120,'Apelco Gen-Mot'),(280,'Apelco Gen-Mto'),(558,'Aprilia'),(117,'Arabella'),(507,'Argo'),(281,'Argo Power'),(491,'Ariel'),(43,'Aro Carpati'),(188,'Aro Corcel'),(137,'Aro Power'),(202,'Asia'),(58,'Asia Motors'),(307,'Aspt'),(220,'Aston'),(122,'Aston Martin'),(189,'Astra'),(627,'Atm'),(492,'Atv'),(119,'Audi'),(475,'Aupaco'),(68,'Austin'),(56,'Auteco'),(195,'Auteco Bajaj'),(222,'Auteco Kawasaki'),(323,'Auteco Kymco'),(282,'Auteco Lambreta'),(283,'Auteco Roa'),(338,'Auteco Victory'),(121,'Auto Bianchi'),(123,'Auto Union'),(69,'Autocar'),(333,'Autocar Azteca'),(225,'Autocar Dc'),(546,'Autocar White'),(547,'Autokraft'),(577,'Avanti'),(124,'Avia'),(217,'Avia Commer'),(669,'Ay250zh-2'),(454,'Ayco'),(284,'B.S.A'),(362,'Babeta'),(113,'Bajaj'),(521,'Barber Greene'),(136,'Barreiros'),(660,'Baw'),(133,'Bedford'),(285,'Beijing'),(363,'Beijing Sanfeng'),(240,'Beisute'),(87,'Belarus'),(538,'Bell'),(130,'Bentley'),(134,'Berliet'),(316,'Best'),(478,'Beta'),(257,'Big'),(205,'Bimota'),(453,'Blaw Knox'),(27,'Bmw'),(223,'Bobcat'),(365,'Bocxod'),(286,'Bombardier'),(259,'Bonser'),(129,'Borg Ward'),(569,'Borgo'),(628,'Bridge'),(668,'Brilliance'),(602,'British'),(128,'Brock Way'),(661,'Bronco'),(435,'Bronto'),(364,'Brownie Tank'),(135,'Bucyrus'),(562,'Buell'),(287,'Bugeni'),(132,'Buggy'),(288,'Buggy Mec Nal'),(242,'Buhin'),(548,'Buhing'),(19,'Buick'),(258,'Bultaco'),(131,'Bussing'),(605,'Buyang'),(471,'Byd'),(20,'Cadillac'),(665,'Cagiva'),(367,'Cain'),(638,'Cam-Am Brp'),(359,'Cameco'),(636,'Can Am '),(673,'Can Am Bombardier'),(674,'Can-Am Brp Bombardier '),(371,'Capacity'),(261,'Carabella'),(369,'Cariaty'),(99,'Caribe'),(60,'Carpati'),(196,'Case'),(65,'Caterpillar'),(372,'Cezet'),(549,'Champion'),(343,'Chana'),(443,'Changan'),(462,'Changhe'),(244,'Changqi'),(430,'Charly'),(126,'Checker'),(80,'Cheroke'),(335,'Chery'),(2,'Chevrolet'),(567,'Chinsu'),(506,'Chongqing'),(553,'Chongqing Zongshen'),(50,'Chrysler'),(39,'Citroen'),(139,'Clark'),(550,'Clase 60 Tx'),(125,'Clausson'),(512,'Cmc'),(603,'Cobra'),(355,'Colmbiana De Chasises'),(183,'Colmove'),(213,'Comando'),(251,'Comet'),(138,'Commer'),(368,'Commuter'),(114,'Concord'),(366,'Cont Mark'),(100,'Corcel'),(329,'Cpi'),(370,'Crown'),(604,'Cushma'),(373,'Dace'),(33,'Dacia'),(208,'Daelim'),(3,'Daewoo'),(140,'Daf'),(40,'Daihatsu'),(325,'Daimler Benz'),(18,'Datsun'),(375,'Dautocar'),(354,'Dayan'),(637,'Dayun'),(70,'De Soto'),(527,'Delkron'),(77,'Delta'),(197,'Demon'),(233,'Derbi'),(530,'Desta'),(374,'Detroit'),(623,'Dfm'),(671,'Dfsk'),(141,'Diamond'),(522,'Dieci'),(75,'Dina'),(83,'Dinapac'),(441,'Dinli'),(221,'Disponible'),(143,'Divico'),(493,'Dkn'),(142,'Dkw'),(10,'Dodge'),(663,'Dong Feng'),(496,'Dresser'),(351,'Ducati'),(472,'Dukar'),(578,'Dyd'),(289,'E.W.D'),(234,'Eagle'),(594,'Eastman'),(103,'Ebro'),(144,'Edsel'),(241,'Elgin'),(309,'Elgin Swwper'),(376,'Encava'),(664,'Energy Motion'),(145,'Ernest Gruber'),(377,'Euclid'),(227,'F.S.O'),(290,'F.S.O Polonez'),(620,'Famy'),(149,'Fanta'),(97,'Fargo'),(150,'Faun'),(147,'Federal'),(28,'Ferrari'),(4,'Fiat'),(291,'Fiat Coupe'),(292,'Fiat Tipo'),(239,'Fiatallis'),(440,'Fibrevan'),(200,'Fiori'),(514,'Firenze'),(378,'Flexible'),(308,'Fona'),(51,'Ford'),(146,'Fordson'),(499,'Formula'),(579,'Foton'),(540,'Fragate'),(148,'Freight Liner'),(390,'Freightliner'),(199,'Frenwich-Yale'),(539,'Fridest'),(379,'Fundamovil'),(526,'Furukawa'),(501,'Futong'),(151,'Fwd'),(560,'Galardi'),(90,'Galion'),(442,'Gas Gas'),(41,'Gaz'),(235,'Gazelle'),(320,'Geely'),(152,'General'),(516,'Genesis'),(326,'Genovo Power'),(76,'Geo'),(385,'Geo Metro'),(293,'Geo-Metro'),(294,'Geo-Tracker'),(381,'Gerlind'),(380,'Ghia'),(360,'Gilera'),(81,'Gmc'),(383,'Gog'),(606,'Gomobile'),(465,'Gomotor'),(382,'Grane Carrier'),(515,'Greatwall'),(607,'Gremlim'),(384,'Grove'),(608,'Gurgel'),(386,'Guzzi'),(651,'Gwm'),(345,'Hafei'),(489,'Halei'),(655,'Haojiang'),(595,'Haojin'),(624,'Hardland'),(55,'Harley Davidson'),(264,'Harmon'),(341,'Hechitools'),(476,'Heli'),(153,'Henry J'),(67,'Hero'),(672,'Higer'),(155,'Hillman'),(71,'Hino'),(231,'Hitachi'),(154,'Hoholden'),(21,'Honda'),(330,'Honlei'),(295,'Hornet'),(156,'Hornet (A.M.C.)'),(95,'Huali'),(497,'Huber'),(157,'Hudson'),(609,'Humbert'),(296,'Hummer'),(632,'Hunter'),(509,'Huonniao'),(79,'Husquarma'),(230,'Husqvarna'),(618,'Hy Motor'),(78,'Hyosung'),(190,'Hyster'),(5,'Hyundai'),(94,'I.F.A'),(505,'Ifg'),(357,'Indian Chief'),(353,'Infiniti'),(451,'Ingersoll Rand'),(198,'Ingersool-Rand'),(57,'International'),(650,'Iota'),(528,'Iseki'),(158,'Islo'),(159,'Iso'),(46,'Isuzu'),(297,'Italjet'),(63,'Iveco'),(347,'Jac'),(32,'Jaguar'),(201,'Jainshe'),(161,'Javeline'),(203,'Jawa'),(344,'Jbwco'),(191,'Jcb'),(47,'Jeep'),(610,'Jepster'),(334,'Jet Ukm'),(450,'Jezhang'),(74,'Jhon Deere'),(387,'Jhonston'),(588,'Jiaj'),(91,'Jialing'),(459,'Jiang Changhe'),(626,'Jiangling'),(480,'Jiangsu'),(502,'Jiangxi Campell Co.'),(349,'Jianshe'),(469,'Jinan'),(105,'Jincheng'),(565,'Jingpin'),(160,'Jiri Sisu'),(599,'Jmc'),(495,'Jmstar'),(564,'Jonway'),(576,'Joyner'),(483,'Junesun'),(585,'Jupiter'),(265,'Kaizer'),(101,'Kamaz'),(611,'Kanabys'),(612,'Kartex'),(448,'Katana'),(555,'Kato'),(53,'Kawasaki'),(474,'Kayak'),(463,'Kazuki'),(542,'Keeway'),(62,'Kenworth'),(6,'Kia'),(216,'Kia Besta'),(298,'Kia Pregio'),(162,'Kiamaster'),(204,'Kimco'),(299,'Kinetic'),(237,'King Linft'),(457,'Kinlon'),(486,'Kinroad'),(551,'Kinroad Xintian'),(340,'Kinroand Xyntian'),(531,'Kobelco'),(207,'Komatsu'),(434,'Koremoto'),(194,'Kraz'),(127,'Krump'),(224,'Ktm'),(84,'Kubota'),(243,'Kymco'),(510,'Kyoto'),(613,'La Salle'),(34,'Lada'),(64,'Lambreta'),(649,'Lambretta'),(163,'Lambrghini'),(14,'Lancia'),(48,'Land Rover'),(535,'Lee Boy'),(300,'Lexus'),(167,'Ley Land'),(445,'Lfa'),(312,'Lifan'),(164,'Lincoln'),(532,'Link Belt'),(248,'Litostrou'),(621,'Liugong'),(670,'Lizhon'),(641,'Lizhong'),(165,'Lloyd'),(262,'Lml'),(328,'Loncin'),(250,'Long'),(654,'Long Cing'),(439,'Lorain'),(166,'Lotus'),(433,'Luas'),(414,'Lula'),(301,'M.G'),(228,'M.G.B.'),(59,'Mack'),(517,'Madvac'),(247,'Magirus Deutz'),(361,'Mahindra'),(168,'Man'),(452,'Marini'),(192,'Marmon'),(388,'Marmon Wester'),(170,'Maserati'),(73,'Massey Ferguzon'),(536,'Matbro'),(447,'Max Motor'),(324,'Max Motors'),(7,'Mazda'),(619,'Mc Cormick '),(477,'Mc Motor'),(339,'Mc Motors'),(659,'Md Bikes'),(29,'Mercedes Benz'),(44,'Mercury'),(639,'Merlo'),(630,'Meta'),(438,'Mg Tf'),(171,'Midget'),(461,'Mini Pocket Bike'),(185,'Minicord'),(66,'Minsk'),(22,'Mitsubishi'),(218,'Mitsubishi Montero'),(96,'Montesa'),(488,'Morgan'),(109,'Morris Austin'),(169,'Moskoovich'),(238,'Motobecane'),(643,'Motokart'),(552,'Motor Uno'),(172,'Muravey'),(482,'Mz'),(107,'Nash'),(598,'Navajo'),(436,'Navistar'),(173,'Neckar'),(508,'Neutral Brand'),(625,'New Era'),(424,'New Hollan'),(276,'New King'),(432,'Nimbus'),(1,'Ninguna'),(15,'Nissan'),(226,'Nissan Patrol'),(337,'Nn'),(229,'Non Plus Ultra'),(302,'Norton'),(342,'Oakland'),(444,'Oklan'),(13,'Oldsmobile'),(174,'Oltat'),(35,'Oltcit'),(525,'Omega'),(45,'Opel'),(428,'Oskosh'),(266,'Osmovile'),(336,'Otis'),(252,'P Y H'),(392,'Pacer'),(175,'Packard'),(187,'Panda'),(394,'Panhard'),(393,'Pannonia'),(629,'Pantera'),(397,'Panzer'),(389,'Parce'),(327,'Passaggio'),(72,'Pegaso'),(267,'Peter Bill'),(256,'Pettibone'),(16,'Peugeot'),(210,'Piaggio'),(614,'Pierce Arrow'),(568,'Pinz Gauer'),(455,'Pionner'),(395,'Piquersa'),(92,'Planeta 3'),(12,'Plymount'),(317,'Plymouth'),(391,'Poclain'),(520,'Polares'),(524,'Polaris Atv'),(177,'Polonez'),(176,'Polowez'),(215,'Polski'),(23,'Pontiac'),(89,'Pony'),(30,'Porshe'),(268,'Prince'),(396,'Prine Skiwan'),(473,'Proling'),(102,'Puch'),(615,'Puma'),(246,'Qingqi'),(591,'Qj'),(112,'Qjiang'),(352,'Raf'),(110,'Rambler'),(245,'Ramirez'),(358,'Range Rover'),(571,'Ranger'),(484,'Rear'),(8,'Renault'),(559,'Reno'),(212,'Reo'),(427,'Revtech'),(446,'Roa'),(178,'Robur'),(398,'Rocar'),(582,'Roketa'),(31,'Rolls Royce'),(400,'Romar'),(399,'Ronart'),(98,'Rover'),(575,'Royal Enfield'),(179,'Saab'),(646,'Sachs '),(481,'Sachs Bikes'),(644,'Saic'),(437,'Saic Chery'),(456,'Saic Wuling'),(500,'Salor'),(402,'Samsung'),(303,'Sanfeng'),(403,'Sanglas'),(346,'Sanya'),(616,'Saturn'),(408,'Sauder'),(270,'Sava'),(181,'Scamel'),(304,'Scania'),(180,'Scania Babis'),(401,'Seahan Bird'),(184,'Seang Yong'),(116,'Seat'),(449,'Shengqibao'),(586,'Shineray Motos'),(460,'Sigma'),(11,'Simca'),(405,'Singer'),(115,'Sisu'),(409,'Siv'),(407,'Skania Vabif'),(458,'Skingo'),(38,'Skoda'),(321,'Skoot'),(319,'Skygo'),(631,'Skymax'),(425,'Smart'),(466,'Smc'),(596,'Songsheng'),(305,'Spartan'),(561,'Special'),(554,'Sport'),(182,'Sreightliner'),(557,'Srfhuaige-Wellai-Qingqi'),(322,'Ssangyong'),(429,'Standar'),(306,'Star'),(523,'Stavostroj'),(332,'Steyr-Puch'),(206,'Studebaker'),(541,'Stutz'),(17,'Subaru'),(601,'Sukida'),(404,'Sumbeam'),(406,'Sumimoto'),(544,'Sumoto'),(269,'Sun Beam'),(209,'Sundiro'),(236,'Sundirro'),(49,'Suzuki'),(574,'Sym'),(106,'T.J.C.'),(511,'T.L. Smith'),(534,'Tadano'),(85,'Tam Deutz'),(572,'Tanaro'),(318,'Tata'),(413,'Tatra'),(566,'Taunus'),(36,'Tavria'),(314,'Tcm'),(647,'Team Ryder'),(416,'Terex'),(487,'Terminator'),(411,'Tga'),(272,'Thames'),(412,'Thomas'),(593,'Thunder Cycle Design'),(468,'Titania'),(656,'Tixxon'),(590,'Tmec'),(415,'Towmotor'),(24,'Toyota'),(410,'Trabant'),(271,'Trader'),(232,'Triumph'),(592,'Triwil'),(634,'Tvs'),(563,'Tyco'),(331,'U.S.W. Motors'),(42,'Uaz'),(490,'Ukm'),(587,'Um'),(479,'Unimog'),(504,'Unison'),(311,'United Motors'),(348,'United Motors Um'),(86,'Universal'),(513,'Ural'),(556,'Usw'),(420,'Valkyrie'),(417,'Vanguard'),(426,'Varzi'),(275,'Vauxhall'),(214,'Vaz'),(419,'Vedette'),(589,'Venom Motors'),(642,'Ventie'),(464,'Vento'),(600,'Verussi'),(255,'Vespa'),(82,'Viasa'),(640,'Victory'),(529,'Vidro-Dynapac'),(633,'Visail'),(667,'Vmoto'),(37,'Volga'),(9,'Volkswagen'),(25,'Volvo'),(418,'Voshud'),(249,'Vosjod'),(485,'Walter'),(494,'Wanxin'),(108,'Wartburg'),(219,'Warzawa'),(421,'Wetern Star'),(273,'White'),(111,'Willco'),(61,'Willys'),(498,'With'),(431,'Wkt'),(356,'Wmk'),(645,'Wrangler'),(617,'Wuling'),(635,'Wuyang'),(467,'Xiali'),(193,'Xingfu'),(533,'Xispa'),(662,'Yakima'),(254,'Yale'),(52,'Yamaha'),(657,'Yamasaky'),(581,'Yamati'),(503,'Yingang'),(186,'Yugo'),(648,'Zastava'),(93,'Zastawa'),(519,'Zephyr'),(104,'Zetor'),(597,'Zhe Jiang Hangcha'),(543,'Zhejiang'),(570,'Zhong Xing'),(423,'Zimmer'),(274,'Ziu'),(313,'Zongshen'),(622,'Zotye'),(260,'Zummy'),(666,'Zundapp'),(422,'Zunk');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moroso`
--

DROP TABLE IF EXISTS `moroso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moroso` (
  `ID_MOROSO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_PERSONA` int(10) unsigned NOT NULL,
  `ESTADO` tinyint(4) NOT NULL,
  PRIMARY KEY (`ID_MOROSO`),
  UNIQUE KEY `ID_MOROSO_UNIQUE` (`ID_MOROSO`),
  KEY `FK_PROPIETARIO_MOROSO_idx` (`ID_PERSONA`),
  CONSTRAINT `FK_PERSONA_MOROSO` FOREIGN KEY (`ID_PERSONA`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moroso`
--

LOCK TABLES `moroso` WRITE;
/*!40000 ALTER TABLE `moroso` DISABLE KEYS */;
/*!40000 ALTER TABLE `moroso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_liquidacion`
--

DROP TABLE IF EXISTS `pago_liquidacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_liquidacion` (
  `ID_PAGO_LIQUIDACION` int(10) unsigned NOT NULL,
  `ANO` year(4) NOT NULL,
  `VALOR_IMPUESTO` decimal(19,4) NOT NULL,
  `VALOR_DESCUENTO` decimal(19,4) NOT NULL,
  `DIAS_MORA` int(11) NOT NULL,
  `VALOR_MORA` decimal(19,4) NOT NULL,
  `VALOR_SEMAFORIZACION` decimal(19,4) NOT NULL,
  `VALOR_SUBTOTAL` decimal(19,4) NOT NULL,
  `ID_HISTORICO` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_PAGO_LIQUIDACION`),
  CONSTRAINT `FK_ID_HISTORICO_PAGO_LIQUIDACION` FOREIGN KEY (`ID_PAGO_LIQUIDACION`) REFERENCES `pago_liquidacion_historico` (`ID_HISTORICO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_liquidacion`
--

LOCK TABLES `pago_liquidacion` WRITE;
/*!40000 ALTER TABLE `pago_liquidacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `pago_liquidacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_liquidacion_historico`
--

DROP TABLE IF EXISTS `pago_liquidacion_historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_liquidacion_historico` (
  `ID_HISTORICO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_PERSONA_NATURAL` int(10) unsigned NOT NULL,
  `ID_VEHICULO` int(10) unsigned NOT NULL,
  `ID_USUARIO` int(10) unsigned NOT NULL,
  `FECHA_LIQUIDA` date NOT NULL,
  `ID_PARAMETRO_IMPUESTO` int(10) unsigned NOT NULL,
  `VALOR_TOTAL` decimal(19,4) DEFAULT NULL,
  `PAGO_LIQUIDACION` tinyint(3) unsigned NOT NULL COMMENT '0 - Pago\n1 - Liquidación',
  PRIMARY KEY (`ID_HISTORICO`),
  UNIQUE KEY `ID_LIQUIDACION_HISTORICO_UNIQUE` (`ID_HISTORICO`),
  KEY `FK_VEHICULO_LIQUIDACION_idx` (`ID_VEHICULO`),
  KEY `FK_USUARIO_LIQUIDACION_idx` (`ID_USUARIO`),
  KEY `FK_CLASE_VEHICULO_LIQUIDACION_idx` (`ID_PARAMETRO_IMPUESTO`),
  KEY `FK_PERSONA_NATURAL_HISTORICO_idx` (`ID_PERSONA_NATURAL`),
  CONSTRAINT `FK_PARAMETRO_IMPUESTO_HISTORICO` FOREIGN KEY (`ID_PARAMETRO_IMPUESTO`) REFERENCES `parametro_impuesto` (`ID_PARAMETRO_IMPUESTO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_PERSONA_NATURAL_HISTORICO` FOREIGN KEY (`ID_PERSONA_NATURAL`) REFERENCES `persona_natural` (`ID_PERSONA_NATURAL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USUARIO_HISTORICO` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario_aplicacion` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_VEHICULO_HISTORICO` FOREIGN KEY (`ID_VEHICULO`) REFERENCES `vehiculo` (`ID_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_liquidacion_historico`
--

LOCK TABLES `pago_liquidacion_historico` WRITE;
/*!40000 ALTER TABLE `pago_liquidacion_historico` DISABLE KEYS */;
/*!40000 ALTER TABLE `pago_liquidacion_historico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametro_aplicacion`
--

DROP TABLE IF EXISTS `parametro_aplicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametro_aplicacion` (
  `ID_PARAMETRO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PARAMETRO` varchar(45) NOT NULL,
  `VALOR_PARAMETRO` text NOT NULL,
  `DESCRIPCION` text,
  PRIMARY KEY (`ID_PARAMETRO`),
  UNIQUE KEY `ID_PARAMETRO_UNIQUE` (`ID_PARAMETRO`),
  UNIQUE KEY `PARAMETRO_UNIQUE` (`PARAMETRO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametro_aplicacion`
--

LOCK TABLES `parametro_aplicacion` WRITE;
/*!40000 ALTER TABLE `parametro_aplicacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `parametro_aplicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parametro_impuesto`
--

DROP TABLE IF EXISTS `parametro_impuesto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parametro_impuesto` (
  `ID_PARAMETRO_IMPUESTO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_VEHICULO` int(10) unsigned NOT NULL,
  `ID_SERVICIO_VEHICULO` int(10) unsigned NOT NULL,
  `VALOR_INICIAL` decimal(19,4) NOT NULL,
  `VALOR_FINAL` decimal(19,4) NOT NULL,
  `PORCENTAJE_IMPUESTO` float NOT NULL,
  `PORCENTAJE_DESCUENTO` float NOT NULL,
  `VALOR_MULTA` decimal(19,4) NOT NULL,
  PRIMARY KEY (`ID_PARAMETRO_IMPUESTO`),
  UNIQUE KEY `ID_CLASE_VEHICULO_UNIQUE` (`ID_PARAMETRO_IMPUESTO`),
  KEY `FK_TIPO_VEHICULO_CLASE_idx` (`ID_TIPO_VEHICULO`),
  KEY `FK_SERVICIO_VEHICULO_CLASE_idx` (`ID_SERVICIO_VEHICULO`),
  CONSTRAINT `FK_SERVICIO_VEHICULO_CLASE` FOREIGN KEY (`ID_SERVICIO_VEHICULO`) REFERENCES `tipo_servicio_vehiculo` (`ID_SERVICIO_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIPO_VEHICULO_CLASE` FOREIGN KEY (`ID_TIPO_VEHICULO`) REFERENCES `tipo_vehiculo` (`ID_TIPO_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parametro_impuesto`
--

LOCK TABLES `parametro_impuesto` WRITE;
/*!40000 ALTER TABLE `parametro_impuesto` DISABLE KEYS */;
INSERT INTO `parametro_impuesto` VALUES (1,1,1,0.0000,40000000.0000,1.5,10,100.0000),(2,1,1,40000001.0000,90000000.0000,2.5,10,200.0000),(3,1,1,90000001.0000,9000000000.0000,3.5,10,300.0000),(4,2,1,0.0000,40000000.0000,2,10,200.0000),(5,2,1,40000001.0000,90000000.0000,3,10,300.0000),(6,2,1,90000001.0000,9000000000.0000,1,20,400.0000),(7,3,1,0.0000,40000000.0000,2,10,200.0000),(8,3,1,40000001.0000,90000000.0000,3,10,300.0000),(9,3,1,90000001.0000,9000000000.0000,4,10,400.0000),(10,4,1,0.0000,40000000.0000,1.5,10,100.0000),(11,4,1,40000001.0000,90000000.0000,3,10,200.0000),(12,4,1,90000001.0000,9000000000.0000,4,10,300.0000),(13,5,1,0.0000,30000000.0000,1,10,50.0000),(14,6,1,0.0000,1000000000.0000,2,10,100.0000),(15,1,2,0.0000,9000000000.0000,1,10,150.0000),(16,8,1,0.0000,9000000000.0000,2,10,200.0000);
/*!40000 ALTER TABLE `parametro_impuesto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfil_usuario`
--

DROP TABLE IF EXISTS `perfil_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `perfil_usuario` (
  `ID_PERFIL` int(10) unsigned NOT NULL,
  `ID_USUARIO` int(10) unsigned NOT NULL,
  KEY `FK_PERFIL_USUARIO_idx` (`ID_PERFIL`),
  KEY `FK_USUARIO_PERFIL_idx` (`ID_USUARIO`),
  CONSTRAINT `FK_PERFIL_USUARIO` FOREIGN KEY (`ID_PERFIL`) REFERENCES `tipo_perfil_usuario` (`ID_TIPO_PERFIL_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USUARIO_PERFIL` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario_aplicacion` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfil_usuario`
--

LOCK TABLES `perfil_usuario` WRITE;
/*!40000 ALTER TABLE `perfil_usuario` DISABLE KEYS */;
INSERT INTO `perfil_usuario` VALUES (1,1),(1,2),(2,1),(2,2),(3,1),(3,2),(4,1),(4,2);
/*!40000 ALTER TABLE `perfil_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `ID_PERSONA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ID_TIPO_DOCUMENTO` int(10) unsigned NOT NULL,
  `NUMERO_DOCUMENTO` varchar(15) NOT NULL,
  `EMAIL` varchar(60) DEFAULT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_PERSONA`),
  UNIQUE KEY `ID_PROPIETARIO_UNIQUE` (`ID_PERSONA`),
  UNIQUE KEY `NUMERO_DOCUMENTO_UNIQUE` (`NUMERO_DOCUMENTO`),
  UNIQUE KEY `EMAIL_UNIQUE` (`EMAIL`),
  KEY `FK_TIPO_DOCUMENTO_PROPIETARIO_idx` (`ID_TIPO_DOCUMENTO`),
  CONSTRAINT `FK_TIPO_DOCUMENTO_PROPIETARIO` FOREIGN KEY (`ID_TIPO_DOCUMENTO`) REFERENCES `tipo_documento` (`ID_TIPO_DOCUMENTO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,2,'99121504282','mateolegi@gmail.com','2017-10-25 20:29:52'),(2,2,'1000000000','eliana_vargas@elpoli.edu.co','2017-11-10 03:41:28'),(6,2,'120000000','prueba2_prueba3@elpoli.edu.co','2017-11-18 01:46:40');
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_juridica`
--

DROP TABLE IF EXISTS `persona_juridica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_juridica` (
  `ID_PERSONA_JURIDICA` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RAZON_SOCIAL` varchar(250) NOT NULL,
  `ID_PERSONA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_PERSONA_JURIDICA`),
  UNIQUE KEY `id_persona_juridica_UNIQUE` (`ID_PERSONA_JURIDICA`),
  KEY `FK_PROPIETARIO_JURIDICA_idx` (`ID_PERSONA`),
  CONSTRAINT `FK_PERSONA_JURIDICA` FOREIGN KEY (`ID_PERSONA`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_juridica`
--

LOCK TABLES `persona_juridica` WRITE;
/*!40000 ALTER TABLE `persona_juridica` DISABLE KEYS */;
/*!40000 ALTER TABLE `persona_juridica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_natural`
--

DROP TABLE IF EXISTS `persona_natural`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_natural` (
  `ID_PERSONA_NATURAL` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PRIMER_NOMBRE` varchar(45) NOT NULL,
  `SEGUNDO_NOMBRE` varchar(45) DEFAULT NULL,
  `PRIMER_APELLIDO` varchar(45) NOT NULL,
  `SEGUNDO_APELLIDO` varchar(45) DEFAULT NULL,
  `FECHA_NACIMIENTO` date NOT NULL,
  `SEXO` varchar(1) NOT NULL,
  `TELEFONO_MOVIL` varchar(15) DEFAULT NULL,
  `ID_PERSONA` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_PERSONA_NATURAL`),
  UNIQUE KEY `id_persona_natural_UNIQUE` (`ID_PERSONA_NATURAL`),
  UNIQUE KEY `ID_PERSONA_UNIQUE` (`ID_PERSONA`),
  CONSTRAINT `FK_PERSONA_NATURAL` FOREIGN KEY (`ID_PERSONA`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_natural`
--

LOCK TABLES `persona_natural` WRITE;
/*!40000 ALTER TABLE `persona_natural` DISABLE KEYS */;
INSERT INTO `persona_natural` VALUES (1,'Mateo',NULL,'Leal','Giraldo','1999-12-15','M','3196550592',1),(2,'Eliana','Maria','Vargas','Borja','2000-01-01','F','+57 3000000000',2),(5,'prueba2','prueba','prueba2','','2000-01-01','F','+57 3000000000',6);
/*!40000 ALTER TABLE `persona_natural` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_vehiculo`
--

DROP TABLE IF EXISTS `persona_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_vehiculo` (
  `ID_PERSONA` int(10) unsigned NOT NULL,
  `ID_VEHICULO` int(10) unsigned NOT NULL,
  KEY `FK_PROPIETARIO_PIVOT_idx` (`ID_PERSONA`),
  KEY `FK_VEHICULO_PIVOT_idx` (`ID_VEHICULO`),
  CONSTRAINT `FK_PERSONA_PIVOT` FOREIGN KEY (`ID_PERSONA`) REFERENCES `persona` (`ID_PERSONA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_VEHICULO_PIVOT` FOREIGN KEY (`ID_VEHICULO`) REFERENCES `vehiculo` (`ID_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_vehiculo`
--

LOCK TABLES `persona_vehiculo` WRITE;
/*!40000 ALTER TABLE `persona_vehiculo` DISABLE KEYS */;
INSERT INTO `persona_vehiculo` VALUES (1,1);
/*!40000 ALTER TABLE `persona_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recurso_usuario`
--

DROP TABLE IF EXISTS `recurso_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recurso_usuario` (
  `ID_RECURSO` int(10) unsigned NOT NULL,
  `ID_USUARIO` int(10) unsigned NOT NULL,
  KEY `FK_RECURSO_USUARIO_idx` (`ID_RECURSO`),
  KEY `FK_USUARIO_RECURSO_idx` (`ID_USUARIO`),
  CONSTRAINT `FK_RECURSO_USUARIO` FOREIGN KEY (`ID_RECURSO`) REFERENCES `tipo_recurso_perfil` (`ID_TIPO_RECURSO_PERFIL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_USUARIO_RECURSO` FOREIGN KEY (`ID_USUARIO`) REFERENCES `usuario_aplicacion` (`ID_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recurso_usuario`
--

LOCK TABLES `recurso_usuario` WRITE;
/*!40000 ALTER TABLE `recurso_usuario` DISABLE KEYS */;
INSERT INTO `recurso_usuario` VALUES (1,1),(1,2),(2,1),(2,2),(3,1),(3,2),(4,1),(4,2),(5,1),(5,2),(6,1),(6,2);
/*!40000 ALTER TABLE `recurso_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reporte`
--

DROP TABLE IF EXISTS `reporte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporte` (
  `ID_REPORTE` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`ID_REPORTE`),
  UNIQUE KEY `ID_REPORTE_UNIQUE` (`ID_REPORTE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reporte`
--

LOCK TABLES `reporte` WRITE;
/*!40000 ALTER TABLE `reporte` DISABLE KEYS */;
/*!40000 ALTER TABLE `reporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `semaforizacion`
--

DROP TABLE IF EXISTS `semaforizacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `semaforizacion` (
  `ID_SEMAFORIZACION` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ANO` year(4) NOT NULL,
  `VALOR` decimal(19,4) NOT NULL,
  PRIMARY KEY (`ID_SEMAFORIZACION`),
  UNIQUE KEY `ID_SEMAFORIZACION_UNIQUE` (`ID_SEMAFORIZACION`),
  UNIQUE KEY `ANO_UNIQUE` (`ANO`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `semaforizacion`
--

LOCK TABLES `semaforizacion` WRITE;
/*!40000 ALTER TABLE `semaforizacion` DISABLE KEYS */;
INSERT INTO `semaforizacion` VALUES (1,2016,45000.0000),(2,2017,50000.0000),(3,2018,49000.0000);
/*!40000 ALTER TABLE `semaforizacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_documento` (
  `ID_TIPO_DOCUMENTO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_DOCUMENTO` varchar(45) NOT NULL,
  `ABREVIACION` varchar(10) NOT NULL,
  PRIMARY KEY (`ID_TIPO_DOCUMENTO`),
  UNIQUE KEY `ID_TIPO_DOCUMENTO_UNIQUE` (`ID_TIPO_DOCUMENTO`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
INSERT INTO `tipo_documento` VALUES (1,'Cédula de ciudadanía','CC'),(2,'Tarjeta de identidad','TI'),(3,'Cédula de extranjería','CE'),(4,'NIT','NIT'),(5,'Registro civil de nacimiento','RC');
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_perfil_usuario`
--

DROP TABLE IF EXISTS `tipo_perfil_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_perfil_usuario` (
  `ID_TIPO_PERFIL_USUARIO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PERFIL` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_TIPO_PERFIL_USUARIO`),
  UNIQUE KEY `ID_PERFIL_USUARIO_UNIQUE` (`ID_TIPO_PERFIL_USUARIO`),
  UNIQUE KEY `PERFIL_UNIQUE` (`PERFIL`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_perfil_usuario`
--

LOCK TABLES `tipo_perfil_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_perfil_usuario` DISABLE KEYS */;
INSERT INTO `tipo_perfil_usuario` VALUES (1,'Administrador'),(3,'Liquidador'),(4,'Pagos'),(2,'Registrador');
/*!40000 ALTER TABLE `tipo_perfil_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_recurso_perfil`
--

DROP TABLE IF EXISTS `tipo_recurso_perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_recurso_perfil` (
  `ID_TIPO_RECURSO_PERFIL` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RECURSO` varchar(45) NOT NULL,
  `ID_PERFIL` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ID_TIPO_RECURSO_PERFIL`),
  KEY `FK_PERFIL_RECURSO_idx` (`ID_PERFIL`),
  CONSTRAINT `FK_PERFIL_RECURSO` FOREIGN KEY (`ID_PERFIL`) REFERENCES `tipo_perfil_usuario` (`ID_TIPO_PERFIL_USUARIO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_recurso_perfil`
--

LOCK TABLES `tipo_recurso_perfil` WRITE;
/*!40000 ALTER TABLE `tipo_recurso_perfil` DISABLE KEYS */;
INSERT INTO `tipo_recurso_perfil` VALUES (1,'Parametrización',1),(2,'Usuarios',1),(3,'Propietarios',2),(4,'Vehículos',2),(5,'Liquidación',3),(6,'Pagos',4);
/*!40000 ALTER TABLE `tipo_recurso_perfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_servicio_vehiculo`
--

DROP TABLE IF EXISTS `tipo_servicio_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_servicio_vehiculo` (
  `ID_SERVICIO_VEHICULO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SERVICIO` varchar(45) NOT NULL,
  PRIMARY KEY (`ID_SERVICIO_VEHICULO`),
  UNIQUE KEY `SERVICIO_UNIQUE` (`SERVICIO`),
  UNIQUE KEY `ID_SERVICIO_VEHICULO_UNIQUE` (`ID_SERVICIO_VEHICULO`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_servicio_vehiculo`
--

LOCK TABLES `tipo_servicio_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_servicio_vehiculo` DISABLE KEYS */;
INSERT INTO `tipo_servicio_vehiculo` VALUES (4,'Diplomático'),(5,'Extranjero'),(3,'Oficial'),(6,'Otro'),(1,'Particular'),(2,'Público');
/*!40000 ALTER TABLE `tipo_servicio_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_vehiculo`
--

DROP TABLE IF EXISTS `tipo_vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_vehiculo` (
  `ID_TIPO_VEHICULO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TIPO_VEHICULO` varchar(45) NOT NULL,
  `DEPRECIACION` float NOT NULL,
  PRIMARY KEY (`ID_TIPO_VEHICULO`),
  UNIQUE KEY `ID_TIPO_VEHICULO_UNIQUE` (`ID_TIPO_VEHICULO`),
  UNIQUE KEY `TIPO_VEHICULO_UNIQUE` (`TIPO_VEHICULO`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_vehiculo`
--

LOCK TABLES `tipo_vehiculo` WRITE;
/*!40000 ALTER TABLE `tipo_vehiculo` DISABLE KEYS */;
INSERT INTO `tipo_vehiculo` VALUES (1,'Automovil',2),(2,'Campero',4),(3,'Camioneta',4),(4,'Van',3),(5,'Motocicleta mayor de 125cc',2.5),(6,'Motocicleta mayor de 600cc',3.5),(7,'Motocicleta menor de 125cc',2),(8,'Carga',10);
/*!40000 ALTER TABLE `tipo_vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_aplicacion`
--

DROP TABLE IF EXISTS `usuario_aplicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_aplicacion` (
  `ID_USUARIO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `NOMBRE_USUARIO` varchar(45) NOT NULL,
  `CONTRASENA` varchar(200) NOT NULL,
  `ID_PERSONA_NATURAL` int(10) unsigned NOT NULL,
  `ESTADO` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`ID_USUARIO`),
  UNIQUE KEY `ID_USUARIO_UNIQUE` (`ID_USUARIO`),
  UNIQUE KEY `NOMBRE_USUARIO_UNIQUE` (`NOMBRE_USUARIO`),
  KEY `FK_PERSONA_NATURAL_USUARIO_idx` (`ID_PERSONA_NATURAL`),
  CONSTRAINT `FK_PERSONA_NATURAL_USUARIO` FOREIGN KEY (`ID_PERSONA_NATURAL`) REFERENCES `persona_natural` (`ID_PERSONA_NATURAL`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_aplicacion`
--

LOCK TABLES `usuario_aplicacion` WRITE;
/*!40000 ALTER TABLE `usuario_aplicacion` DISABLE KEYS */;
INSERT INTO `usuario_aplicacion` VALUES (1,'mateolegi','contrasena',1,1),(2,'eliana','contrasena',2,1),(4,'prueba','contrasena',5,0);
/*!40000 ALTER TABLE `usuario_aplicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiculo` (
  `ID_VEHICULO` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PLACA` varchar(45) NOT NULL,
  `ID_LINEA` int(10) unsigned NOT NULL,
  `ID_TIPO_SERVICIO` int(10) unsigned NOT NULL,
  `ID_TIPO_VEHICULO` int(10) unsigned NOT NULL,
  `MODELO` year(4) NOT NULL,
  `VALOR` decimal(19,4) NOT NULL,
  `FECHA_REGISTRO` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_VEHICULO`),
  UNIQUE KEY `ID_VEHICULO_UNIQUE` (`ID_VEHICULO`),
  UNIQUE KEY `PLACA_UNIQUE` (`PLACA`),
  KEY `FK_LINEA_VEHICULO_idx` (`ID_LINEA`),
  KEY `FK_SERVICIO_VEHICULO_idx` (`ID_TIPO_SERVICIO`),
  KEY `FK_TIPO_VEHICULO_idx` (`ID_TIPO_VEHICULO`),
  CONSTRAINT `FK_LINEA_VEHICULO` FOREIGN KEY (`ID_LINEA`) REFERENCES `linea` (`ID_LINEA`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_SERVICIO_VEHICULO` FOREIGN KEY (`ID_TIPO_SERVICIO`) REFERENCES `tipo_servicio_vehiculo` (`ID_SERVICIO_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIPO_VEHICULO` FOREIGN KEY (`ID_TIPO_VEHICULO`) REFERENCES `tipo_vehiculo` (`ID_TIPO_VEHICULO`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
INSERT INTO `vehiculo` VALUES (1,'FCK666',6,1,1,2018,160000000.0000,'2017-11-21 00:31:34'),(2,'YNF854',1,1,1,2006,60000000.0000,'2017-11-21 00:31:34'),(3,'RGE465',20,2,1,2000,54500000.0000,'2017-11-21 00:31:34'),(4,'WER412',16,2,1,2016,76350000.0000,'2017-11-21 00:31:34'),(5,'FAP348',10,1,1,2005,20000000.0000,'2017-11-21 00:31:34'),(6,'ASD986',5,1,1,2003,27895000.0000,'2017-11-21 00:31:34'),(7,'TTB561',21,1,1,2001,100000000.0000,'2017-11-21 00:31:34'),(8,'VGH655',2,1,1,2017,65850000.0000,'2017-11-21 00:31:34'),(9,'VHG248',8,1,1,2004,85531000.0000,'2017-11-21 00:31:34'),(10,'ERD159',7,1,1,2014,55520000.0000,'2017-11-21 00:31:34'),(11,'AAA114',1,1,1,2017,93100000.0000,'2017-11-28 13:10:15');
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-28 10:32:16
