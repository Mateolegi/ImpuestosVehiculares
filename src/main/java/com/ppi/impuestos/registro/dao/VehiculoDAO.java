package com.ppi.impuestos.registro.dao;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.Vehiculo;

public class VehiculoDAO extends CrudDAO<Vehiculo> {
	public VehiculoDAO() {
		super(Vehiculo.class);
	}
	
	public Vehiculo findByPlaca(String placa) {
		return entityManager.createNamedQuery("Vehiculo.findByPlaca", Vehiculo.class)
				.setParameter("placa", placa)
				.getSingleResult();
	}
}
