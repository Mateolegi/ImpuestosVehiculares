package com.ppi.impuestos.registro.dto;

import java.math.BigDecimal;

import com.ppi.impuestos.core.dto.DTO;
import com.ppi.impuestos.gerencia.dto.TipoServicioVehiculoDTO;
import com.ppi.impuestos.gerencia.dto.TipoVehiculoDTO;

public class VehiculoDTO implements DTO {

	private int idVehiculo;
	private String placa;
	private LineaDTO linea;
	private TipoServicioVehiculoDTO tipoServicio;
	private TipoVehiculoDTO tipoVehiculo;
	private int modelo;
	private BigDecimal valor;
	private String fechaRegistro;
	
	public int getIdVehiculo() {
		return idVehiculo;
	}
	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public LineaDTO getLinea() {
		return linea;
	}
	public void setLinea(LineaDTO linea) {
		this.linea = linea;
	}
	public TipoServicioVehiculoDTO getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(TipoServicioVehiculoDTO tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public TipoVehiculoDTO getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(TipoVehiculoDTO tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
}
