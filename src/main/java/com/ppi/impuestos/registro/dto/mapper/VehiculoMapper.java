package com.ppi.impuestos.registro.dto.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.Vehiculo;
import com.ppi.impuestos.core.util.Constants;
import com.ppi.impuestos.gerencia.dto.mapper.TipoServicioVehiculoMapper;
import com.ppi.impuestos.gerencia.dto.mapper.TipoVehiculoMapper;
import com.ppi.impuestos.registro.dto.VehiculoDTO;

public class VehiculoMapper {

	public static VehiculoDTO toDTO(Vehiculo entity) {
		final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		final VehiculoDTO dto = new VehiculoDTO();
		dto.setIdVehiculo(entity.getIdVehiculo());
		dto.setPlaca(entity.getPlaca());
		dto.setLinea(LineaMapper.toDTO(entity.getLinea()));
		dto.setTipoServicio(TipoServicioVehiculoMapper.toDTO(entity.getTipoServicioVehiculo()));
		dto.setTipoVehiculo(TipoVehiculoMapper.toDTO(entity.getTipoVehiculo()));
		dto.setModelo(entity.getModelo().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear());
		dto.setValor(entity.getValor());
		dto.setFechaRegistro(formatter.format(entity.getFechaRegistro()));
		return dto;
	}

	public static List<VehiculoDTO> toDTOList(List<Vehiculo> entities) {
		List<VehiculoDTO> list = new ArrayList<>();
		for(Vehiculo entity: entities) list.add(toDTO(entity));
		return list;
	}
}
