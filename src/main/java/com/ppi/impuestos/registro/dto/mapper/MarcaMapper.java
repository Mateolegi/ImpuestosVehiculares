package com.ppi.impuestos.registro.dto.mapper;

import com.ppi.impuestos.core.entity.Marca;
import com.ppi.impuestos.registro.dto.MarcaDTO;

public class MarcaMapper {

	public static MarcaDTO toDTO(Marca entity) {
		final MarcaDTO dto = new MarcaDTO();
		dto.setIdMarca(entity.getIdMarca());
		dto.setMarca(entity.getMarca());
		return dto;
	}
	
	public static Marca toEntity(MarcaDTO dto) {
		final Marca entity = new Marca();
		entity.setMarca(dto.getMarca());
		return entity;
	}
}
