package com.ppi.impuestos.registro.dto;

import com.ppi.impuestos.core.dto.DTO;

public class LineaDTO implements DTO {

	private int idLinea;
	private String linea;
	private MarcaDTO marca;
	
	public int getIdLinea() {
		return idLinea;
	}
	public void setIdLinea(int idLinea) {
		this.idLinea = idLinea;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	public MarcaDTO getMarca() {
		return marca;
	}
	public void setMarca(MarcaDTO marca) {
		this.marca = marca;
	}
}
