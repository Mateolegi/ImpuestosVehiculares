package com.ppi.impuestos.registro.dto;

import com.ppi.impuestos.core.dto.DTO;

public class MarcaDTO implements DTO {

	private int idMarca;
	private String marca;
	
	public int getIdMarca() {
		return idMarca;
	}
	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
}
