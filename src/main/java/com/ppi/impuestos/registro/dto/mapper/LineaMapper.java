package com.ppi.impuestos.registro.dto.mapper;

import com.ppi.impuestos.core.entity.Linea;
import com.ppi.impuestos.registro.dto.LineaDTO;

public class LineaMapper {

	public static LineaDTO toDTO(Linea entity) {
		final LineaDTO dto = new LineaDTO();
		dto.setIdLinea(entity.getIdLinea());
		dto.setLinea(entity.getLinea());
		dto.setMarca(MarcaMapper.toDTO(entity.getMarca()));
		return dto;
	}
}
