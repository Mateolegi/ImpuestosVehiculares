package com.ppi.impuestos.registro.manager;

import java.math.BigDecimal;
import java.time.ZoneId;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.Vehiculo;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.registro.dao.VehiculoDAO;
import com.ppi.impuestos.registro.dto.VehiculoDTO;
import com.ppi.impuestos.registro.dto.mapper.VehiculoMapper;

import javassist.NotFoundException;

public class VehiculoManager implements CrudManager {
	
//	private static final Logger LOGGER = LogManager.getLogger(VehiculoManager.class);
	private VehiculoDAO dao = new VehiculoDAO();
	private Gson gson;
	
	public VehiculoManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#findAll()
	 */
	@Override
	public String findAll(UriInfo uriInfo) {
		String placa = uriInfo.getQueryParameters().getFirst("placa");
		if (placa != null) {
			return gson.toJson(VehiculoMapper.toDTO(dao.findByPlaca(placa)));
		}
		return gson.toJson(VehiculoMapper.toDTOList(dao.findAll()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#find(int)
	 */
	@Override
	public String find(int id) throws NotFoundException {
		Vehiculo vehiculo = dao.find(id);
		if(vehiculo == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(VehiculoMapper.toDTO(vehiculo));
	}

	/**
	 * Calcula el valor depreciado del vehículo para el año. <br>
	 * El avalúo del vehículo se calcula restando el 2% del 
	 * valor del vehículo por cada año desde el registrado en 
	 * el modelo. <br>
	 * Por ejemplo, para un vehículo de 2015 con un valor de 
	 * $60.000.000,00 su valor depreciado para cada año después de 
	 * 2015 será: <br>
	 * 2016 -> 60.000.000 - (60.000.000 * 0.02) = 58.800.000 <br>
	 * 2017 -> 58.800.000 - (58.800.000 * 0.02) = 57.624.000 <br>
	 * @param vehiculo vehículo que se calculará su depreciación
	 * @param annoActual ano hasta el que se calcula la depreciación
	 * @return valor depreciado del vehículo
	 */
	public BigDecimal calcularDepreciacion(Vehiculo vehiculo, int annoActual) {
		BigDecimal avaluo = vehiculo.getValor();
		if (vehiculo.getModelo().toInstant().atZone(ZoneId.systemDefault()).getYear() >= annoActual) {
			return vehiculo.getValor();
		} else {
			int anno = vehiculo.getModelo().toInstant().atZone(ZoneId.systemDefault()).getYear();
			while(anno <= annoActual) {
				avaluo = avaluo.subtract(avaluo.multiply(new BigDecimal(0.02)));
				anno++;
			}
			return avaluo;
		}
	}

	public Response save(VehiculoDTO dto, UriInfo uriInfo) {
//		try {	
//			Vehiculo entity = dao.save(VehiculoMapper.toEntity(dto));
//			URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(entity.getIdVehiculo())).build();		
//			return Response.created(uri).entity(gson.toJson(VehiculoMapper.toDTO(entity))).build();
//		} catch (Exception e) {
//			LOGGER.error(e);
//			return Response.serverError().build();
//		}
		return Response.status(Response.Status.NOT_IMPLEMENTED).build();
	}

	public Response update (int id, VehiculoDTO dto) {
//		Vehiculo entity = VehiculoMapper.toEntity(dto);
//		entity.setIdVehiculo(id);
//		return gson.toJson(VehiculoMapper.toDTO(dao.update(entity)));
		return Response.status(Response.Status.NOT_IMPLEMENTED).build();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#delete(int)
	 */
	@Override
	public Response delete(int id) {
		dao.delete(dao.find(id));
		if(dao.find(id) == null) return Response.noContent().build();
		return Response.serverError().build();
	}

}
