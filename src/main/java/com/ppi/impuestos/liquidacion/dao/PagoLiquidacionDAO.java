package com.ppi.impuestos.liquidacion.dao;

import java.time.ZoneId;
import java.util.List;

import javax.persistence.Query;
import javax.transaction.Transactional;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.PagoLiquidacion;

public class PagoLiquidacionDAO extends CrudDAO<PagoLiquidacion> {

	public PagoLiquidacionDAO() {
		super(PagoLiquidacion.class);
	}

	@Override
	@Transactional
	public PagoLiquidacion save(PagoLiquidacion entity) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO pago_liquidacion ")
		   .append("(ANO, DIAS_MORA, VALOR_DESCUENTO, VALOR_IMPUESTO, VALOR_MORA, VALOR_SEMAFORIZACION, VALOR_SUBTOTAL, ")
		   .append("ID_HISTORICO) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
		Query query = entityManager.createNativeQuery(sql.toString(), PagoLiquidacion.class)
				.setParameter(1, entity.getAno().toInstant().atZone(ZoneId.systemDefault()).getYear())
				.setParameter(2, entity.getDiasMora())
				.setParameter(3, entity.getValorDescuento().toString())
				.setParameter(4, entity.getValorImpuesto().toString())
				.setParameter(5, entity.getValorMora().toString())
				.setParameter(6, entity.getValorSemaforizacion().toString())
				.setParameter(7, entity.getValorSubtotal())
				.setParameter(8, entity.getPagoLiquidacionHistorico().getIdHistorico());
		query.getResultList();
		List<PagoLiquidacion> list = findAll();
		return list.get(list.size()-1);
	}
}
