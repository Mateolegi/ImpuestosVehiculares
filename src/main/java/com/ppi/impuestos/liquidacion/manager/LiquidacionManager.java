package com.ppi.impuestos.liquidacion.manager;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.dao.SemaforizacionDAO;
import com.ppi.impuestos.core.entity.FechaDescuento;
import com.ppi.impuestos.core.entity.FechaPago;
import com.ppi.impuestos.core.entity.PagoLiquidacion;
import com.ppi.impuestos.core.entity.PagoLiquidacionHistorico;
import com.ppi.impuestos.core.entity.ParametroImpuesto;
import com.ppi.impuestos.core.entity.PersonaNatural;
import com.ppi.impuestos.core.entity.Semaforizacion;
import com.ppi.impuestos.core.entity.UsuarioAplicacion;
import com.ppi.impuestos.core.entity.Vehiculo;
import com.ppi.impuestos.core.util.KeyUtils;
import com.ppi.impuestos.gerencia.dao.ParametroImpuestoDAO;
import com.ppi.impuestos.gerencia.dao.PersonaNaturalDAO;
import com.ppi.impuestos.gerencia.dao.UsuarioAplicacionDAO;
import com.ppi.impuestos.liquidacion.dto.mapper.HistoricoMapper;
import com.ppi.impuestos.registro.dao.VehiculoDAO;
import com.ppi.impuestos.registro.manager.VehiculoManager;

import io.jsonwebtoken.Jwts;
import javassist.NotFoundException;

public class LiquidacionManager {
	
	private CrudDAO<PagoLiquidacionHistorico> dao = new CrudDAO<>(PagoLiquidacionHistorico.class);
	private Gson gson;
	private final Date actualDate = new Date();

	public LiquidacionManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	public String findAll(UriInfo uriInfo, String authorizationToken) {
		String idVehiculo = uriInfo.getQueryParameters().getFirst("vehiculo");
		String idPersona = uriInfo.getQueryParameters().getFirst("persona");
		if (idVehiculo != null || idPersona != null) {
			return gson.toJson(HistoricoMapper.toDTO(generarHistorico(idVehiculo, idPersona, authorizationToken)));
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Inicia el proceso de generación de liquidación.<br>
	 * Verifica que el vehículo y la persona existan.<br>
	 * Si el vehículo nunca ha realizado liquidaciones o pagos de impuesto, 
	 * se le generará un registro desde la primera fecha de la tabla 
	 * FECHA_PAGO, posteriormente se procede a la generación del histórico.
	 * Si el histórico se almacenó correctamente se genera cada elemento del 
	 * histórico.
	 * @param idVehiculo identificador del vehículo - que se usará para 
	 * verificar su existencia en la base de datos
	 * @param idPersona identificador de la persona natural - que se usará 
	 * para verificar su existencia en la base de datos
	 * @param authorizationToken token JWT que usa el usuario para enviar 
	 * peticiones - se usa para asignar el usuario a la liquidación
	 * @return histórico generado
	 */
	private PagoLiquidacionHistorico generarHistorico(String idVehiculo, String idPersona, String authorizationToken) {
		Vehiculo vehiculo = new VehiculoDAO().find(Integer.parseInt(idVehiculo));
		PersonaNatural personaNatural = new PersonaNaturalDAO().find(Integer.parseInt(idPersona));
		if (vehiculo == null || personaNatural == null) 
			throw new BadRequestException("El vehículo o la persona no se encuentran");
		List<PagoLiquidacionHistorico> pagosLiquidaciones = vehiculo.getPagoLiquidacionHistoricos();
		if (pagosLiquidaciones == null || pagosLiquidaciones.isEmpty()) {
			PagoLiquidacionHistorico historico = createHistorico(personaNatural, vehiculo, authorizationToken);
			if (historico != null) {
				List<PagoLiquidacion> historial = new ArrayList<>();
				for (FechaPago fechaPago: new CrudDAO<>(FechaPago.class).findAll()) {
					if (fechaPago.getFechaPago().toInstant().atZone(ZoneId.systemDefault()).getYear() >= vehiculo
							.getModelo().toInstant().atZone(ZoneId.systemDefault()).getYear()) {
						historial.add(createLiquidacion(fechaPago, vehiculo, historico));
					}
				}
				historico.setPagoLiquidacions(historial);
				historico.setValorTotal(getValorTotal(historial));
//				return dao.update(historico);
				return historico;
			} else {
				throw new ServerErrorException(500);
			}
		} else {
//			for (PagoLiquidacionHistorico liquidacion: pagosLiquidaciones) {
//				// Si es una liquidación
//				if (liquidacion.getPagoLiquidacion() == 0) {
//					// TODO
//				}
//			}
			return null;
		}
	}
	
	private PagoLiquidacionHistorico createHistorico(PersonaNatural personaNatural, Vehiculo vehiculo, 
			String authorizationToken) {
		PagoLiquidacionHistorico historico = new PagoLiquidacionHistorico();
		historico.setPersonaNatural(personaNatural);
		historico.setVehiculo(vehiculo);
		historico.setUsuarioAplicacion(getUsuario(authorizationToken));
		historico.setFechaLiquida(actualDate);
		historico.setParametroImpuesto(getParametroImpuesto(vehiculo));
//		return dao.save(historico);
		return historico;
	}
	
	private BigDecimal getValorTotal(List<PagoLiquidacion> pagosLiquidaciones) {
		BigDecimal suma = BigDecimal.ZERO;
		for (PagoLiquidacion pagoLiquidacion: pagosLiquidaciones) 
			suma = suma.add(pagoLiquidacion.getValorSubtotal());
		return suma;
	}
	
	private UsuarioAplicacion getUsuario(String authorizationToken) {
		authorizationToken = authorizationToken.substring("Bearer".length()).trim();
		try {
			int idUsuario = Integer.parseInt(Jwts.parser().setSigningKey(KeyUtils.KEY)
					.parseClaimsJws(authorizationToken).getBody().getId());
			return new UsuarioAplicacionDAO().find(idUsuario);
		} catch (Exception e) {
			throw new BadRequestException("Error procesando el token");
		}
	}
	
	private PagoLiquidacion createLiquidacion(FechaPago fechaPago, Vehiculo vehiculo, 
			PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		PagoLiquidacion liquidacion = new PagoLiquidacion();
		// Obtiene el año
		liquidacion.setAno(getAno(fechaPago));
		// Obtiene el valor del impuesto
		liquidacion.setValorImpuesto(getValorImpuesto(vehiculo, pagoLiquidacionHistorico.getParametroImpuesto(), 
				liquidacion.getAno().toInstant().atZone(ZoneId.systemDefault()).getYear()));
		// Obtiene el valor del descuento
		liquidacion.setValorDescuento(getValorDescuento(actualDate, liquidacion.getAno().toInstant()
				.atZone(ZoneId.systemDefault()).getYear(), liquidacion.getValorImpuesto(), vehiculo, 
				pagoLiquidacionHistorico.getParametroImpuesto()));
		// Milisegundos entre la fecha actual y la fecha límite de pago de la fecha que se está iterando
		long diasMora = actualDate.getTime() - fechaPago.getFechaPago().getTime();
		// Se calcula los días de mora y se redondea al mayor
		liquidacion.setDiasMora(Math.round(diasMora/(1000 * 60 * 60 * 24)));
		// Obtiene el valor de la mora
		liquidacion.setValorMora(getValorMora(liquidacion.getDiasMora(), vehiculo, 
				pagoLiquidacionHistorico.getParametroImpuesto()));
		// Obtiene el valor de la semaforización
		liquidacion.setValorSemaforizacion(getValorSemaforizacion(liquidacion.getAno().toInstant()
				.atZone(ZoneId.systemDefault()).getYear()));
		// Obtiene el valor subtotal
		liquidacion.setValorSubtotal(getValorSubtotal(liquidacion));
		liquidacion.setPagoLiquidacionHistorico(pagoLiquidacionHistorico);
//		return new PagoLiquidacionDAO().save(liquidacion);
		return liquidacion;
	}
	
	private Date getAno(FechaPago fechaPago) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(Calendar.YEAR, fechaPago.getFechaPago().toInstant().atZone(ZoneId.systemDefault()).getYear());
		return calendar.getTime();
	}

	private BigDecimal getValorSubtotal(PagoLiquidacion pagoLiquidacion) {
		return pagoLiquidacion.getValorImpuesto()
				.subtract(pagoLiquidacion.getValorDescuento())
				.add(pagoLiquidacion.getValorMora())
				.add(pagoLiquidacion.getValorSemaforizacion());
	}
	
	private BigDecimal getValorSemaforizacion(int year) {
		Semaforizacion semaforizacion = new SemaforizacionDAO().findByYear(year);
		if (semaforizacion != null)
			return semaforizacion.getValor();
		return null;
	}
	
	private BigDecimal getValorMora(int diasMora, Vehiculo vehiculo, ParametroImpuesto parametro) {
		BigDecimal valorMulta = parametro.getValorMulta();
		if (diasMora <= 0) {
			return BigDecimal.ZERO;
		}
		return valorMulta.multiply(BigDecimal.valueOf(diasMora));
	}
	
	private ParametroImpuesto getParametroImpuesto(Vehiculo vehiculo) {
		List<ParametroImpuesto> parametros = new ParametroImpuestoDAO()
				.findByServicioTipo(vehiculo.getTipoServicioVehiculo(), vehiculo.getTipoVehiculo());
		for (ParametroImpuesto parametro: parametros) {
			// Compara si el valor del vehículo es mayor o igual al valor inicial del parámetro
			 boolean mayorIgual = vehiculo.getValor().compareTo(parametro.getValorInicial()) == 1 
					|| vehiculo.getValor().compareTo(parametro.getValorInicial()) == 0;
			// Compara si el valor del vehículo es menor o igual al valor final del parámetro
			boolean menorIgual = vehiculo.getValor().compareTo(parametro.getValorFinal()) == -1 
					|| vehiculo.getValor().compareTo(parametro.getValorFinal()) == 0;
			if (mayorIgual && menorIgual) {
				return parametro;
			}
		}
		return null;
	}
	
	private BigDecimal getValorDescuento(Date actualDate, int year, BigDecimal valorImpuesto, Vehiculo vehiculo,
			ParametroImpuesto parametro) {
		List<FechaDescuento> fechasDescuento = new CrudDAO<>(FechaDescuento.class).findAll();
		for (FechaDescuento fechaDescuento: fechasDescuento) {
			if (fechaDescuento.getFechaDescuento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getYear() == year) {
				if (actualDate.after(fechaDescuento.getFechaDescuento())) {
					return BigDecimal.ZERO;
				} else {
					return valorImpuesto.multiply(BigDecimal.valueOf(parametro.getPorcentajeDescuento()/100));
				}
			}
		}
		return null;
	}
	
	/**
	 * Calcula el valor del impuesto
	 * @param vehiculo
	 * @param parametro
	 * @param anno
	 * @return
	 */
	private BigDecimal getValorImpuesto(Vehiculo vehiculo, ParametroImpuesto parametro, int anno) {
		return new VehiculoManager().calcularDepreciacion(vehiculo, anno)
				.multiply(BigDecimal.valueOf(parametro.getPorcentajeImpuesto()/100));
	}

	public String find(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	public Response delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
