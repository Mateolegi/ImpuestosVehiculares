package com.ppi.impuestos.liquidacion.dto;

import java.math.BigDecimal;

public class LiquidacionDTO {

	private int idLiquidacion;
	private int ano;
	private BigDecimal valorImpuesto;
	private BigDecimal valorDescuento;
	private int diasMora;
	private BigDecimal valorMora;
	private BigDecimal valorSemaforizacion;
	private BigDecimal valorSubtotal;

	public int getIdLiquidacion() {
		return idLiquidacion;
	}
	public void setIdLiquidacion(int idLiquidacion) {
		this.idLiquidacion = idLiquidacion;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
	public BigDecimal getValorImpuesto() {
		return valorImpuesto;
	}
	public void setValorImpuesto(BigDecimal valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}
	public BigDecimal getValorDescuento() {
		return valorDescuento;
	}
	public void setValorDescuento(BigDecimal valorDescuento) {
		this.valorDescuento = valorDescuento;
	}
	public int getDiasMora() {
		return diasMora;
	}
	public void setDiasMora(int diasMora) {
		this.diasMora = diasMora;
	}
	public BigDecimal getValorMora() {
		return valorMora;
	}
	public void setValorMora(BigDecimal valorMora) {
		this.valorMora = valorMora;
	}
	public BigDecimal getValorSemaforizacion() {
		return valorSemaforizacion;
	}
	public void setValorSemaforizacion(BigDecimal valorSemaforizacion) {
		this.valorSemaforizacion = valorSemaforizacion;
	}
	public BigDecimal getValorSubtotal() {
		return valorSubtotal;
	}
	public void setValorSubtotal(BigDecimal valorSubtotal) {
		this.valorSubtotal = valorSubtotal;
	}
}
