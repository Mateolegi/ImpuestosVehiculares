package com.ppi.impuestos.liquidacion.dto.mapper;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.PagoLiquidacion;
import com.ppi.impuestos.liquidacion.dto.LiquidacionDTO;

public class LiquidacionMapper {

	private LiquidacionMapper() { }
	
	public static LiquidacionDTO toDTO(PagoLiquidacion entity) {
		LiquidacionDTO dto = new LiquidacionDTO();
		dto.setIdLiquidacion(entity.getIdPagoLiquidacion());
		dto.setAno(entity.getAno().toInstant().atZone(ZoneId.systemDefault()).getYear());
		dto.setValorImpuesto(entity.getValorImpuesto());
		dto.setValorDescuento(entity.getValorDescuento());
		dto.setDiasMora(entity.getDiasMora());
		dto.setValorMora(entity.getValorMora());
		dto.setValorSemaforizacion(entity.getValorSemaforizacion());
		dto.setValorSubtotal(entity.getValorSubtotal());
		return dto;
	}

	public static List<LiquidacionDTO> toDTOList(List<PagoLiquidacion> entities) {
		List<LiquidacionDTO> list = new ArrayList<>();
		for(PagoLiquidacion entity: entities) list.add(toDTO(entity));
		return list;
	}
}
