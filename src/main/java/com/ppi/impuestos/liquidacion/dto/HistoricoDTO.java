package com.ppi.impuestos.liquidacion.dto;

import java.math.BigDecimal;
import java.util.List;

import com.ppi.impuestos.gerencia.dto.ParametroImpuestoDTO;
import com.ppi.impuestos.gerencia.dto.PersonaNaturalDTO;
import com.ppi.impuestos.gerencia.dto.UsuarioAplicacionDTO;
import com.ppi.impuestos.registro.dto.VehiculoDTO;

public class HistoricoDTO {

	private int idHistorial;
	private PersonaNaturalDTO personaNatural;
	private VehiculoDTO vehiculo;
	private UsuarioAplicacionDTO usuario;
	private String fechaLiquida;
	private ParametroImpuestoDTO parametroImpuesto;
	private List<LiquidacionDTO> historial;
	private BigDecimal valorTotal;

	public int getIdHistorial() {
		return idHistorial;
	}
	public void setIdHistorial(int idHistorial) {
		this.idHistorial = idHistorial;
	}
	public PersonaNaturalDTO getPersonaNatural() {
		return personaNatural;
	}
	public void setPersonaNatural(PersonaNaturalDTO personaNatural) {
		this.personaNatural = personaNatural;
	}
	public VehiculoDTO getVehiculo() {
		return vehiculo;
	}
	public void setVehiculo(VehiculoDTO vehiculo) {
		this.vehiculo = vehiculo;
	}
	public UsuarioAplicacionDTO getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioAplicacionDTO usuario) {
		this.usuario = usuario;
	}
	public String getFechaLiquida() {
		return fechaLiquida;
	}
	public void setFechaLiquida(String fechaLiquida) {
		this.fechaLiquida = fechaLiquida;
	}
	public ParametroImpuestoDTO getParametroImpuesto() {
		return parametroImpuesto;
	}
	public void setParametroImpuesto(ParametroImpuestoDTO parametroImpuesto) {
		this.parametroImpuesto = parametroImpuesto;
	}
	public List<LiquidacionDTO> getHistorial() {
		return historial;
	}
	public void setHistorial(List<LiquidacionDTO> historial) {
		this.historial = historial;
	}
	public BigDecimal getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}
}
