package com.ppi.impuestos.liquidacion.dto.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.ppi.impuestos.core.entity.PagoLiquidacionHistorico;
import com.ppi.impuestos.core.util.Constants;
import com.ppi.impuestos.gerencia.dto.mapper.ParametroImpuestoMapper;
import com.ppi.impuestos.gerencia.dto.mapper.PersonaNaturalMapper;
import com.ppi.impuestos.gerencia.dto.mapper.UsuarioAplicacionMapper;
import com.ppi.impuestos.liquidacion.dto.HistoricoDTO;
import com.ppi.impuestos.registro.dto.mapper.VehiculoMapper;

public class HistoricoMapper {

	private HistoricoMapper() { }
	
	public static HistoricoDTO toDTO(PagoLiquidacionHistorico entity) {
		final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		HistoricoDTO dto = new HistoricoDTO();
		dto.setIdHistorial(entity.getIdHistorico());
		dto.setPersonaNatural(PersonaNaturalMapper.toDTO(entity.getPersonaNatural()));
		dto.setVehiculo(VehiculoMapper.toDTO(entity.getVehiculo()));
		dto.setUsuario(UsuarioAplicacionMapper.toDTO(entity.getUsuarioAplicacion()));
		dto.setFechaLiquida(formatter.format(entity.getFechaLiquida()));
		dto.setParametroImpuesto(ParametroImpuestoMapper.toDTO(entity.getParametroImpuesto()));
		dto.setHistorial(LiquidacionMapper.toDTOList(entity.getPagoLiquidacions()));
		dto.setValorTotal(entity.getValorTotal());
		return dto;
	}
}
