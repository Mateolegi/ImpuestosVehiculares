package com.ppi.impuestos.core.dao;

import com.ppi.impuestos.core.entity.TipoDocumento;

public class TipoDocumentoDAO extends CrudDAO<TipoDocumento> {
	public TipoDocumentoDAO() {
		super(TipoDocumento.class);
	}
}
