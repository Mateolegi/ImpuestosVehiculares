package com.ppi.impuestos.core.dao;

import com.ppi.impuestos.core.entity.FechaDescuento;

public class FechaDescuentoDAO extends CrudDAO<FechaDescuento> {
	public FechaDescuentoDAO() {
		super(FechaDescuento.class);
	}
}
