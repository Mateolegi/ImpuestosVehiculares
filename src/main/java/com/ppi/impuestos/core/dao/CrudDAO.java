package com.ppi.impuestos.core.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Capa de acceso a datos genérica
 * @author Mateo Leal
 * @param <E> Clase de la entidad
 */
public class CrudDAO<E> {
	
	/** Interactúa con la base de datos */
	protected EntityManager entityManager ;
	/** Obtiene las conexiones de la base de datos */
	protected EntityManagerFactory entityManagerFactory;
	/** Entidad de JPA */
	private final Class<E> clazz;
	
	/**
	 * Inicializa el EntityManager y setea la clase que se usará 
	 * para el mapeo de los datos obtenido de la base de datos
	 * @param clazz entidad de JPA
	 */
	public CrudDAO(Class<E> clazz) {
		entityManagerFactory = Persistence.createEntityManagerFactory("impuestos");
		entityManager = entityManagerFactory.createEntityManager();
		this.clazz = clazz;
	}

	/**
	 * Obtiene todos los registros de una tabla
	 * @return lista con todos los registros de la tabla
	 */
	public List<E> findAll() {
		return entityManager.createNamedQuery(clazz.getSimpleName() + ".findAll", clazz).getResultList();
	}
	
	/**
	 * Obtiene una entidad basada en su identificador
	 * @param id id de la entidad
	 * @return entidad
	 */
	public E find(int id) {
		return entityManager.find(clazz, id);
	}
	
	/**
	 * Almacena una entidad en el contexto de persistencia
	 * @param entity entidad que se va a almacenar
	 */
	public E save(E entity) {
		entityManager.getTransaction().begin();
		entityManager.persist(entity);
		entityManager.getTransaction().commit();
		List<E> list = findAll();
		return list.get(list.size()-1);
	}
	
	/**
	 * Actualiza una entidad
	 * @param entity entidad que se va a actualizar
	 * @return entidad actualizada
	 */
	public E update(E entity) {
		entityManager.getTransaction().begin();
		E updatedEntity = entityManager.merge(entity);
		entityManager.getTransaction().commit();
		return updatedEntity;
	}
	
	/**
	 * Elimina una entidad del contexto de persistencia
	 * @param entity entidad que se va a eliminar
	 */
	public void delete(E entity) {
		entityManager.getTransaction().begin();
		entityManager.remove(entity);
		entityManager.getTransaction().commit();
	}
}
