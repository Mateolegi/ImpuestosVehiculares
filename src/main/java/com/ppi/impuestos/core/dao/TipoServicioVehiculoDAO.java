package com.ppi.impuestos.core.dao;

import com.ppi.impuestos.core.entity.TipoServicioVehiculo;

public class TipoServicioVehiculoDAO extends CrudDAO<TipoServicioVehiculo> {
	public TipoServicioVehiculoDAO() {
		super(TipoServicioVehiculo.class);
	}
}
