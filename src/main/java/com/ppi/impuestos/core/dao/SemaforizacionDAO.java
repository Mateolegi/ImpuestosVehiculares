package com.ppi.impuestos.core.dao;

import com.ppi.impuestos.core.entity.Semaforizacion;

public class SemaforizacionDAO extends CrudDAO<Semaforizacion> {
	public SemaforizacionDAO() {
		super(Semaforizacion.class);
	}

	public Semaforizacion findByYear(int year) {
		return (Semaforizacion) entityManager.createNativeQuery("SELECT * FROM SEMAFORIZACION WHERE ANO = '" + year + "'", 
				Semaforizacion.class).getSingleResult();
	}
}
