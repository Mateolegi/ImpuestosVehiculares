package com.ppi.impuestos.core.dao;

import com.ppi.impuestos.core.entity.TipoVehiculo;

public class TipoVehiculoDAO extends CrudDAO<TipoVehiculo> {
	public TipoVehiculoDAO() {
		super(TipoVehiculo.class);
	}
}
