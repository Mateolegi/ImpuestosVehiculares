package com.ppi.impuestos.core.exception;

public class AttributeNotValidException extends Exception {

	private static final long serialVersionUID = 7393336793986754291L;

	public AttributeNotValidException() {
		super();
	}
	
	public AttributeNotValidException(String message) {
		super(message);
	}
}
