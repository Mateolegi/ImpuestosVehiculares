package com.ppi.impuestos.core.exception;

public class IncorrectFileException extends Exception {

	private static final long serialVersionUID = -325488785065728893L;
	
	public IncorrectFileException() {
		super();
	}
	
	public IncorrectFileException(String message) {
		super(message);
	}
}
