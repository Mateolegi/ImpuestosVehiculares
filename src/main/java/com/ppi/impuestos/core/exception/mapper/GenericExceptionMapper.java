package com.ppi.impuestos.core.exception.mapper;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.Severity;

import com.ppi.impuestos.core.exception.IncorrectFileException;
import com.ppi.impuestos.core.exception.entity.ErrorMessage;

@Provider
public class GenericExceptionMapper implements ExceptionMapper<Throwable> {
	
	private static final Logger LOGGER = LogManager.getLogger(GenericExceptionMapper.class);

	/*
	 * (non-Javadoc)
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(Throwable exception) {
		LOGGER.warn(exception);
		exception.printStackTrace();
		ErrorMessage errorMessage = new ErrorMessage();
		if(exception instanceof IncorrectFileException) {
			errorMessage.setErrorCode(Status.BAD_REQUEST);
			errorMessage.setSeverity(Severity.FATAL);
		} if (exception instanceof BadRequestException) {
			errorMessage.setErrorCode(Status.BAD_REQUEST);
			errorMessage.setSeverity(Severity.WARNING);
		} if (exception instanceof ProcessingException) {
			errorMessage.setErrorCode(Status.BAD_REQUEST);
			errorMessage.setSeverity(Severity.WARNING);
		} else {
			errorMessage.setErrorCode(Status.INTERNAL_SERVER_ERROR);
			errorMessage.setSeverity(Severity.FATAL);
		}
		if(exception.getMessage() == null) 
			errorMessage.setErrorMessage("An internal error occurred");
		else 
			errorMessage.setErrorMessage(exception.getMessage().replace('"', '\''));
		return Response.status(errorMessage.getErrorCode()).entity(errorMessage).build();
	}
}
