package com.ppi.impuestos.core.exception.entity;

import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.Severity;

@XmlRootElement
public class ErrorMessage {

	private String errorMessage;
	private Status errorCode;
	private Severity severity;
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public Status getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(Status errorCode) {
		this.errorCode = errorCode;
	}
	public Severity getSeverity() {
		return severity;
	}
	public void setSeverity(Severity severity) {
		this.severity = severity;
	}
}
