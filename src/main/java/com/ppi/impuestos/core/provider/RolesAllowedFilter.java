package com.ppi.impuestos.core.provider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Priority;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.Severity;

import com.ppi.impuestos.core.entity.TipoRecursoPerfil;
import com.ppi.impuestos.core.entity.UsuarioAplicacion;
import com.ppi.impuestos.core.exception.entity.ErrorMessage;
import com.ppi.impuestos.core.util.KeyUtils;
import com.ppi.impuestos.gerencia.dao.UsuarioAplicacionDAO;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class RolesAllowedFilter implements ContainerRequestFilter {

	@Context
	private ResourceInfo resourceInfo;
	private static final Logger LOGGER = LogManager.getLogger(RolesAllowedFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			// Get the HTTP Authorization header from the request
	        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
	        LOGGER.info("Encabezado de autentificación: " + authorizationHeader);
	        // Check if the HTTP Authorization header is present and formatted correctly
	        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
	        	LOGGER.warn("Encabezado de autentificación inválido: " + authorizationHeader);
	            throw new NotAuthorizedException("Debe tener un encabezado de autentificación");
	        }
	        // Extract the token from the HTTP Authorization header
	        String token = authorizationHeader.substring("Bearer".length()).trim();
	        try {
	            // Validate the token
	            Claims claims = Jwts.parser().setSigningKey(KeyUtils.KEY).parseClaimsJws(token).getBody();
	            UsuarioAplicacion usuarioAplicacion = new UsuarioAplicacionDAO().find(Integer.parseInt(claims.getId()));
	            if(usuarioAplicacion != null && usuarioAplicacion.getNombreUsuario().equals(claims.getSubject())) {
	            	RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
	            	Set<String> hashSetRolesAnnotation = new HashSet<>(Arrays.asList(rolesAnnotation.value()));
	            	Set<String> hasSetRolesPermitidoUsuario = new HashSet<>();
	            	for(TipoRecursoPerfil recursoPerfil: usuarioAplicacion.getTipoRecursoPerfils()) {
	            		hasSetRolesPermitidoUsuario.add(recursoPerfil.getRecurso());
	            	}
	            	for (String rolMetodo : hashSetRolesAnnotation) {
	            		if (hasSetRolesPermitidoUsuario.contains(rolMetodo)) {
	            			return;
	            		}
	            	}
	            	ErrorMessage errorMessage = new ErrorMessage();
	            	errorMessage.setErrorCode(Response.Status.FORBIDDEN);
	            	errorMessage.setErrorMessage("No tienes permiso para acceder a este recurso");
	            	errorMessage.setSeverity(Severity.WARNING);
	            	requestContext.abortWith(Response.status(Response.Status.FORBIDDEN).entity(errorMessage).build());
	            	return;
	            } else {
	            	LOGGER.warn("#### invalid token : " + token);
	            	throw new NotAuthorizedException("Authorization header must be provided");
	            }
	        } catch (Exception e) {
	        	LOGGER.warn("#### invalid token : " + token);
	            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	        }
		}
	}
}
