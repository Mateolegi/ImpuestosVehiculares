package com.ppi.impuestos.core.provider;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.Severity;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.core.exception.entity.ErrorMessage;
import com.ppi.impuestos.core.util.KeyUtils;

import io.jsonwebtoken.Jwts;

@Provider
@JWTTokenNeeded
@Priority(Priorities.AUTHENTICATION)
public class JWTTokenNeededFilter implements ContainerRequestFilter {

	private static final Logger LOGGER = LogManager.getLogger(JWTTokenNeededFilter.class);

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		ErrorMessage errorMessage = new ErrorMessage();
    	errorMessage.setErrorCode(Response.Status.UNAUTHORIZED);
    	errorMessage.setErrorMessage("No se encontró un token de autentificación");
    	errorMessage.setSeverity(Severity.WARNING);
		// Obtiene el authorization header de la solicitud
        String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        LOGGER.info("Authorization Header: " + authorizationHeader);
        // Revisa si el HTTP Authorization header está presente y en un formato correcto
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
        	LOGGER.warn("Authorization Header inválido: " + authorizationHeader);
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage).build());
            return;
        }
        // Extrae el token de la HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();
        LOGGER.info(requestContext.getUriInfo().getPath());
        try {
            // Valida el token
            Jwts.parser().setSigningKey(KeyUtils.KEY).parseClaimsJws(token);
            LOGGER.info("Token válido: " + token);
        } catch (Exception e) {
        	LOGGER.warn("Token inválido: " + token);
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage).build());
            return;
        }
	}
}
