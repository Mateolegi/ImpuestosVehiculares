package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the pago_liquidacion database table.
 * 
 */
@Entity
@Table(name="pago_liquidacion")
@NamedQuery(name="PagoLiquidacion.findAll", query="SELECT p FROM PagoLiquidacion p")
public class PagoLiquidacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PAGO_LIQUIDACION", unique=true, nullable=false)
	private int idPagoLiquidacion;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date ano;

	@Column(name="DIAS_MORA", nullable=false)
	private int diasMora;

	@Column(name="VALOR_DESCUENTO", nullable=false, precision=19, scale=4)
	private BigDecimal valorDescuento;

	@Column(name="VALOR_IMPUESTO", nullable=false, precision=19, scale=4)
	private BigDecimal valorImpuesto;

	@Column(name="VALOR_MORA", nullable=false, precision=19, scale=4)
	private BigDecimal valorMora;

	@Column(name="VALOR_SEMAFORIZACION", nullable=false, precision=19, scale=4)
	private BigDecimal valorSemaforizacion;

	@Column(name="VALOR_SUBTOTAL", nullable=false, precision=19, scale=4)
	private BigDecimal valorSubtotal;

	//bi-directional many-to-one association to PagoLiquidacionHistorico
	@ManyToOne
	@JoinColumn(name="ID_HISTORICO", nullable=false)
	private PagoLiquidacionHistorico pagoLiquidacionHistorico;

	public PagoLiquidacion() {
	}

	public int getIdPagoLiquidacion() {
		return this.idPagoLiquidacion;
	}

	public void setIdPagoLiquidacion(int idPagoLiquidacion) {
		this.idPagoLiquidacion = idPagoLiquidacion;
	}

	public Date getAno() {
		return this.ano;
	}

	public void setAno(Date ano) {
		this.ano = ano;
	}

	public int getDiasMora() {
		return this.diasMora;
	}

	public void setDiasMora(int diasMora) {
		this.diasMora = diasMora;
	}

	public BigDecimal getValorDescuento() {
		return this.valorDescuento;
	}

	public void setValorDescuento(BigDecimal valorDescuento) {
		this.valorDescuento = valorDescuento;
	}

	public BigDecimal getValorImpuesto() {
		return this.valorImpuesto;
	}

	public void setValorImpuesto(BigDecimal valorImpuesto) {
		this.valorImpuesto = valorImpuesto;
	}

	public BigDecimal getValorMora() {
		return this.valorMora;
	}

	public void setValorMora(BigDecimal valorMora) {
		this.valorMora = valorMora;
	}

	public BigDecimal getValorSemaforizacion() {
		return this.valorSemaforizacion;
	}

	public void setValorSemaforizacion(BigDecimal valorSemaforizacion) {
		this.valorSemaforizacion = valorSemaforizacion;
	}

	public BigDecimal getValorSubtotal() {
		return this.valorSubtotal;
	}

	public void setValorSubtotal(BigDecimal valorSubtotal) {
		this.valorSubtotal = valorSubtotal;
	}

	public PagoLiquidacionHistorico getPagoLiquidacionHistorico() {
		return this.pagoLiquidacionHistorico;
	}

	public void setPagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		this.pagoLiquidacionHistorico = pagoLiquidacionHistorico;
	}

}