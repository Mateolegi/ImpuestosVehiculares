package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usuario_aplicacion database table.
 * 
 */
@Entity
@Table(name="usuario_aplicacion")
@NamedQueries({
	@NamedQuery(name="UsuarioAplicacion.findAll", query="SELECT u FROM UsuarioAplicacion u ORDER BY u.idUsuario"),
	@NamedQuery(name="UsuarioAplicacion.findByUsername", query="SELECT u FROM UsuarioAplicacion u WHERE u.nombreUsuario = :username")
})
public class UsuarioAplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_USUARIO", unique=true, nullable=false)
	private int idUsuario;

	@Column(nullable=false, length=200)
	private String contrasena;

	@Column(nullable=false)
	private byte estado;

	@Column(name="NOMBRE_USUARIO", nullable=false, length=45)
	private String nombreUsuario;

	//bi-directional many-to-one association to PagoLiquidacionHistorico
	@OneToMany(mappedBy="usuarioAplicacion")
	private List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos;

	//bi-directional many-to-many association to TipoPerfilUsuario
	@ManyToMany
	@JoinTable(
		name="perfil_usuario"
		, joinColumns={
			@JoinColumn(name="ID_USUARIO", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_PERFIL", nullable=false)
			}
		)
	private List<TipoPerfilUsuario> tipoPerfilUsuarios;

	//bi-directional many-to-many association to TipoRecursoPerfil
	@ManyToMany
	@JoinTable(
		name="recurso_usuario"
		, joinColumns={
			@JoinColumn(name="ID_USUARIO", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_RECURSO", nullable=false)
			}
		)
	private List<TipoRecursoPerfil> tipoRecursoPerfils;

	//bi-directional one-to-one association to PersonaNatural
	@OneToOne
	@JoinColumn(name="ID_PERSONA_NATURAL", nullable=false)
	private PersonaNatural personaNatural;

	public UsuarioAplicacion() {
	}

	public int getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getContrasena() {
		return this.contrasena;
	}

	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

	public byte getEstado() {
		return this.estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public String getNombreUsuario() {
		return this.nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public List<PagoLiquidacionHistorico> getPagoLiquidacionHistoricos() {
		return this.pagoLiquidacionHistoricos;
	}

	public void setPagoLiquidacionHistoricos(List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos) {
		this.pagoLiquidacionHistoricos = pagoLiquidacionHistoricos;
	}

	public PagoLiquidacionHistorico addPagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().add(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setUsuarioAplicacion(this);

		return pagoLiquidacionHistorico;
	}

	public PagoLiquidacionHistorico removePagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().remove(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setUsuarioAplicacion(null);

		return pagoLiquidacionHistorico;
	}

	public List<TipoPerfilUsuario> getTipoPerfilUsuarios() {
		return this.tipoPerfilUsuarios;
	}

	public void setTipoPerfilUsuarios(List<TipoPerfilUsuario> tipoPerfilUsuarios) {
		this.tipoPerfilUsuarios = tipoPerfilUsuarios;
	}

	public List<TipoRecursoPerfil> getTipoRecursoPerfils() {
		return this.tipoRecursoPerfils;
	}

	public void setTipoRecursoPerfils(List<TipoRecursoPerfil> tipoRecursoPerfils) {
		this.tipoRecursoPerfils = tipoRecursoPerfils;
	}

	public PersonaNatural getPersonaNatural() {
		return this.personaNatural;
	}

	public void setPersonaNatural(PersonaNatural personaNatural) {
		this.personaNatural = personaNatural;
	}

}