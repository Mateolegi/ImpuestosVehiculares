package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the fecha_pago database table.
 * 
 */
@Entity
@Table(name="fecha_pago")
@NamedQuery(name="FechaPago.findAll", query="SELECT f FROM FechaPago f")
public class FechaPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_FECHA_PAGO", unique=true, nullable=false)
	private int idFechaPago;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_PAGO", nullable=false)
	private Date fechaPago;

	public FechaPago() {
	}

	public int getIdFechaPago() {
		return this.idFechaPago;
	}

	public void setIdFechaPago(int idFechaPago) {
		this.idFechaPago = idFechaPago;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

}