package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the vehiculo database table.
 * 
 */
@Entity
@Table(name="vehiculo")
@NamedQueries({
	@NamedQuery(name="Vehiculo.findAll", query="SELECT v FROM Vehiculo v ORDER BY v.idVehiculo ASC"),
	@NamedQuery(name="Vehiculo.findByPlaca", query="SELECT v FROM Vehiculo v WHERE v.placa = :placa")
})
public class Vehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_VEHICULO", unique=true, nullable=false)
	private int idVehiculo;

	@Column(name="FECHA_REGISTRO", nullable=false)
	private Timestamp fechaRegistro;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date modelo;

	@Column(nullable=false, length=45)
	private String placa;

	@Column(nullable=false, precision=19, scale=4)
	private BigDecimal valor;

	//bi-directional many-to-one association to PagoLiquidacionHistorico
	@OneToMany(mappedBy="vehiculo")
	private List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos;

	//bi-directional many-to-many association to Persona
	@ManyToMany(mappedBy="vehiculos")
	private List<Persona> personas;

	//bi-directional many-to-one association to Linea
	@ManyToOne
	@JoinColumn(name="ID_LINEA", nullable=false)
	private Linea linea;

	//bi-directional many-to-one association to TipoServicioVehiculo
	@ManyToOne
	@JoinColumn(name="ID_TIPO_SERVICIO", nullable=false)
	private TipoServicioVehiculo tipoServicioVehiculo;

	//bi-directional many-to-one association to TipoVehiculo
	@ManyToOne
	@JoinColumn(name="ID_TIPO_VEHICULO", nullable=false)
	private TipoVehiculo tipoVehiculo;

	public Vehiculo() {
	}

	public int getIdVehiculo() {
		return this.idVehiculo;
	}

	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Date getModelo() {
		return this.modelo;
	}

	public void setModelo(Date modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return this.placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public List<PagoLiquidacionHistorico> getPagoLiquidacionHistoricos() {
		return this.pagoLiquidacionHistoricos;
	}

	public void setPagoLiquidacionHistoricos(List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos) {
		this.pagoLiquidacionHistoricos = pagoLiquidacionHistoricos;
	}

	public PagoLiquidacionHistorico addPagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().add(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setVehiculo(this);

		return pagoLiquidacionHistorico;
	}

	public PagoLiquidacionHistorico removePagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().remove(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setVehiculo(null);

		return pagoLiquidacionHistorico;
	}

	public List<Persona> getPersonas() {
		return this.personas;
	}

	public void setPersonas(List<Persona> personas) {
		this.personas = personas;
	}

	public Linea getLinea() {
		return this.linea;
	}

	public void setLinea(Linea linea) {
		this.linea = linea;
	}

	public TipoServicioVehiculo getTipoServicioVehiculo() {
		return this.tipoServicioVehiculo;
	}

	public void setTipoServicioVehiculo(TipoServicioVehiculo tipoServicioVehiculo) {
		this.tipoServicioVehiculo = tipoServicioVehiculo;
	}

	public TipoVehiculo getTipoVehiculo() {
		return this.tipoVehiculo;
	}

	public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

}