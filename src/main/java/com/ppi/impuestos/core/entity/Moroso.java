package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the moroso database table.
 * 
 */
@Entity
@Table(name="moroso")
@NamedQuery(name="Moroso.findAll", query="SELECT m FROM Moroso m")
public class Moroso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MOROSO", unique=true, nullable=false)
	private int idMoroso;

	@Column(nullable=false)
	private byte estado;

	//bi-directional many-to-one association to Persona
	@ManyToOne
	@JoinColumn(name="ID_PERSONA", nullable=false)
	private Persona persona;

	public Moroso() {
	}

	public int getIdMoroso() {
		return this.idMoroso;
	}

	public void setIdMoroso(int idMoroso) {
		this.idMoroso = idMoroso;
	}

	public byte getEstado() {
		return this.estado;
	}

	public void setEstado(byte estado) {
		this.estado = estado;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}