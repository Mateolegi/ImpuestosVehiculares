package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the fecha_descuento database table.
 * 
 */
@Entity
@Table(name="fecha_descuento")
@NamedQuery(name="FechaDescuento.findAll", query="SELECT f FROM FechaDescuento f")
public class FechaDescuento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_FECHA_DESCUENTO", unique=true, nullable=false)
	private int idFechaDescuento;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_DESCUENTO", nullable=false)
	private Date fechaDescuento;

	public FechaDescuento() {
	}

	public int getIdFechaDescuento() {
		return this.idFechaDescuento;
	}

	public void setIdFechaDescuento(int idFechaDescuento) {
		this.idFechaDescuento = idFechaDescuento;
	}

	public Date getFechaDescuento() {
		return this.fechaDescuento;
	}

	public void setFechaDescuento(Date fechaDescuento) {
		this.fechaDescuento = fechaDescuento;
	}

}