package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_vehiculo database table.
 * 
 */
@Entity
@Table(name="tipo_vehiculo")
@NamedQuery(name="TipoVehiculo.findAll", query="SELECT t FROM TipoVehiculo t ORDER BY t.idTipoVehiculo ASC")
public class TipoVehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_VEHICULO", unique=true, nullable=false)
	private int idTipoVehiculo;

	@Column(nullable=false)
	private float depreciacion;

	@Column(name="TIPO_VEHICULO", nullable=false, length=45)
	private String tipoVehiculo;

	//bi-directional many-to-one association to ParametroImpuesto
	@OneToMany(mappedBy="tipoVehiculo")
	private List<ParametroImpuesto> parametroImpuestos;

	//bi-directional many-to-one association to Vehiculo
	@OneToMany(mappedBy="tipoVehiculo")
	private List<Vehiculo> vehiculos;

	public TipoVehiculo() {
	}

	public int getIdTipoVehiculo() {
		return this.idTipoVehiculo;
	}

	public void setIdTipoVehiculo(int idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	public float getDepreciacion() {
		return this.depreciacion;
	}

	public void setDepreciacion(float depreciacion) {
		this.depreciacion = depreciacion;
	}

	public String getTipoVehiculo() {
		return this.tipoVehiculo;
	}

	public void setTipoVehiculo(String tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public List<ParametroImpuesto> getParametroImpuestos() {
		return this.parametroImpuestos;
	}

	public void setParametroImpuestos(List<ParametroImpuesto> parametroImpuestos) {
		this.parametroImpuestos = parametroImpuestos;
	}

	public ParametroImpuesto addParametroImpuesto(ParametroImpuesto parametroImpuesto) {
		getParametroImpuestos().add(parametroImpuesto);
		parametroImpuesto.setTipoVehiculo(this);

		return parametroImpuesto;
	}

	public ParametroImpuesto removeParametroImpuesto(ParametroImpuesto parametroImpuesto) {
		getParametroImpuestos().remove(parametroImpuesto);
		parametroImpuesto.setTipoVehiculo(null);

		return parametroImpuesto;
	}

	public List<Vehiculo> getVehiculos() {
		return this.vehiculos;
	}

	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public Vehiculo addVehiculo(Vehiculo vehiculo) {
		getVehiculos().add(vehiculo);
		vehiculo.setTipoVehiculo(this);

		return vehiculo;
	}

	public Vehiculo removeVehiculo(Vehiculo vehiculo) {
		getVehiculos().remove(vehiculo);
		vehiculo.setTipoVehiculo(null);

		return vehiculo;
	}

}