package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the marca database table.
 * 
 */
@Entity
@Table(name="marca")
@NamedQuery(name="Marca.findAll", query="SELECT m FROM Marca m")
public class Marca implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MARCA", unique=true, nullable=false)
	private int idMarca;

	@Column(nullable=false, length=45)
	private String marca;

	//bi-directional many-to-one association to Linea
	@OneToMany(mappedBy="marca")
	private List<Linea> lineas;

	public Marca() {
	}

	public int getIdMarca() {
		return this.idMarca;
	}

	public void setIdMarca(int idMarca) {
		this.idMarca = idMarca;
	}

	public String getMarca() {
		return this.marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public List<Linea> getLineas() {
		return this.lineas;
	}

	public void setLineas(List<Linea> lineas) {
		this.lineas = lineas;
	}

	public Linea addLinea(Linea linea) {
		getLineas().add(linea);
		linea.setMarca(this);

		return linea;
	}

	public Linea removeLinea(Linea linea) {
		getLineas().remove(linea);
		linea.setMarca(null);

		return linea;
	}

}