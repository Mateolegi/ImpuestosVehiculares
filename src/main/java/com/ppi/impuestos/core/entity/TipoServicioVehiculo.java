package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_servicio_vehiculo database table.
 * 
 */
@Entity
@Table(name="tipo_servicio_vehiculo")
@NamedQuery(name="TipoServicioVehiculo.findAll", query="SELECT t FROM TipoServicioVehiculo t ORDER BY t.idServicioVehiculo")
public class TipoServicioVehiculo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_SERVICIO_VEHICULO", unique=true, nullable=false)
	private int idServicioVehiculo;

	@Column(nullable=false, length=45)
	private String servicio;

	//bi-directional many-to-one association to ParametroImpuesto
	@OneToMany(mappedBy="tipoServicioVehiculo")
	private List<ParametroImpuesto> parametroImpuestos;

	//bi-directional many-to-one association to Vehiculo
	@OneToMany(mappedBy="tipoServicioVehiculo")
	private List<Vehiculo> vehiculos;

	public TipoServicioVehiculo() {
	}

	public int getIdServicioVehiculo() {
		return this.idServicioVehiculo;
	}

	public void setIdServicioVehiculo(int idServicioVehiculo) {
		this.idServicioVehiculo = idServicioVehiculo;
	}

	public String getServicio() {
		return this.servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public List<ParametroImpuesto> getParametroImpuestos() {
		return this.parametroImpuestos;
	}

	public void setParametroImpuestos(List<ParametroImpuesto> parametroImpuestos) {
		this.parametroImpuestos = parametroImpuestos;
	}

	public ParametroImpuesto addParametroImpuesto(ParametroImpuesto parametroImpuesto) {
		getParametroImpuestos().add(parametroImpuesto);
		parametroImpuesto.setTipoServicioVehiculo(this);

		return parametroImpuesto;
	}

	public ParametroImpuesto removeParametroImpuesto(ParametroImpuesto parametroImpuesto) {
		getParametroImpuestos().remove(parametroImpuesto);
		parametroImpuesto.setTipoServicioVehiculo(null);

		return parametroImpuesto;
	}

	public List<Vehiculo> getVehiculos() {
		return this.vehiculos;
	}

	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public Vehiculo addVehiculo(Vehiculo vehiculo) {
		getVehiculos().add(vehiculo);
		vehiculo.setTipoServicioVehiculo(this);

		return vehiculo;
	}

	public Vehiculo removeVehiculo(Vehiculo vehiculo) {
		getVehiculos().remove(vehiculo);
		vehiculo.setTipoServicioVehiculo(null);

		return vehiculo;
	}

}