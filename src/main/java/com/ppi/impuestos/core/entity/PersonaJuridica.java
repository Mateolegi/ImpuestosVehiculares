package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the persona_juridica database table.
 * 
 */
@Entity
@Table(name="persona_juridica")
@NamedQuery(name="PersonaJuridica.findAll", query="SELECT p FROM PersonaJuridica p")
public class PersonaJuridica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERSONA_JURIDICA", unique=true, nullable=false)
	private int idPersonaJuridica;

	@Column(name="RAZON_SOCIAL", nullable=false, length=250)
	private String razonSocial;

	//bi-directional one-to-one association to Persona
	@OneToOne
	@JoinColumn(name="ID_PERSONA", nullable=false)
	private Persona persona;

	public PersonaJuridica() {
	}

	public int getIdPersonaJuridica() {
		return this.idPersonaJuridica;
	}

	public void setIdPersonaJuridica(int idPersonaJuridica) {
		this.idPersonaJuridica = idPersonaJuridica;
	}

	public String getRazonSocial() {
		return this.razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}