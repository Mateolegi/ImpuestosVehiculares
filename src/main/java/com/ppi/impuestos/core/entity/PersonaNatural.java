package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the persona_natural database table.
 * 
 */
@Entity
@Table(name="persona_natural")
@NamedQuery(name="PersonaNatural.findAll", query="SELECT p FROM PersonaNatural p")
public class PersonaNatural implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERSONA_NATURAL", unique=true, nullable=false)
	private int idPersonaNatural;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_NACIMIENTO", nullable=false)
	private Date fechaNacimiento;

	@Column(name="PRIMER_APELLIDO", nullable=false, length=45)
	private String primerApellido;

	@Column(name="PRIMER_NOMBRE", nullable=false, length=45)
	private String primerNombre;

	@Column(name="SEGUNDO_APELLIDO", length=45)
	private String segundoApellido;

	@Column(name="SEGUNDO_NOMBRE", length=45)
	private String segundoNombre;

	@Column(nullable=false, length=1)
	private String sexo;

	@Column(name="TELEFONO_MOVIL", length=15)
	private String telefonoMovil;

	//bi-directional many-to-one association to PagoLiquidacionHistorico
	@OneToMany(mappedBy="personaNatural")
	private List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos;

	//bi-directional one-to-one association to UsuarioAplicacion
	@OneToOne(mappedBy="personaNatural")
	private UsuarioAplicacion usuarioAplicacion;

	//bi-directional one-to-one association to Persona
	@OneToOne
	@JoinColumn(name="ID_PERSONA", nullable=false)
	private Persona persona;

	public PersonaNatural() {
	}

	public int getIdPersonaNatural() {
		return this.idPersonaNatural;
	}

	public void setIdPersonaNatural(int idPersonaNatural) {
		this.idPersonaNatural = idPersonaNatural;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getPrimerApellido() {
		return this.primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getPrimerNombre() {
		return this.primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoApellido() {
		return this.segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getSegundoNombre() {
		return this.segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getSexo() {
		return this.sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getTelefonoMovil() {
		return this.telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public List<PagoLiquidacionHistorico> getPagoLiquidacionHistoricos() {
		return this.pagoLiquidacionHistoricos;
	}

	public void setPagoLiquidacionHistoricos(List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos) {
		this.pagoLiquidacionHistoricos = pagoLiquidacionHistoricos;
	}

	public PagoLiquidacionHistorico addPagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().add(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setPersonaNatural(this);

		return pagoLiquidacionHistorico;
	}

	public PagoLiquidacionHistorico removePagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().remove(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setPersonaNatural(null);

		return pagoLiquidacionHistorico;
	}

	public UsuarioAplicacion getUsuarioAplicacion() {
		return this.usuarioAplicacion;
	}

	public void setUsuarioAplicacion(UsuarioAplicacion usuarioAplicacion) {
		this.usuarioAplicacion = usuarioAplicacion;
	}

	public Persona getPersona() {
		return this.persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}