package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the parametro_impuesto database table.
 * 
 */
@Entity
@Table(name="parametro_impuesto")
@NamedQueries({
	@NamedQuery(name="ParametroImpuesto.findAll", query="SELECT p FROM ParametroImpuesto p ORDER BY p.idParametroImpuesto ASC"),
	@NamedQuery(name="ParametroImpuesto.findByServicioTipo", query="SELECT p FROM ParametroImpuesto p WHERE p.tipoVehiculo = :tipo AND p.tipoServicioVehiculo = :servicio")
})
public class ParametroImpuesto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PARAMETRO_IMPUESTO", unique=true, nullable=false)
	private int idParametroImpuesto;

	@Column(name="PORCENTAJE_DESCUENTO", nullable=false)
	private float porcentajeDescuento;

	@Column(name="PORCENTAJE_IMPUESTO", nullable=false)
	private float porcentajeImpuesto;

	@Column(name="VALOR_FINAL", nullable=false, precision=19, scale=4)
	private BigDecimal valorFinal;

	@Column(name="VALOR_INICIAL", nullable=false, precision=19, scale=4)
	private BigDecimal valorInicial;

	@Column(name="VALOR_MULTA", nullable=false, precision=19, scale=4)
	private BigDecimal valorMulta;

	//bi-directional many-to-one association to PagoLiquidacionHistorico
	@OneToMany(mappedBy="parametroImpuesto")
	private List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos;

	//bi-directional many-to-one association to TipoServicioVehiculo
	@ManyToOne
	@JoinColumn(name="ID_SERVICIO_VEHICULO", nullable=false)
	private TipoServicioVehiculo tipoServicioVehiculo;

	//bi-directional many-to-one association to TipoVehiculo
	@ManyToOne
	@JoinColumn(name="ID_TIPO_VEHICULO", nullable=false)
	private TipoVehiculo tipoVehiculo;

	public ParametroImpuesto() {
	}

	public int getIdParametroImpuesto() {
		return this.idParametroImpuesto;
	}

	public void setIdParametroImpuesto(int idParametroImpuesto) {
		this.idParametroImpuesto = idParametroImpuesto;
	}

	public float getPorcentajeDescuento() {
		return this.porcentajeDescuento;
	}

	public void setPorcentajeDescuento(float porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}

	public float getPorcentajeImpuesto() {
		return this.porcentajeImpuesto;
	}

	public void setPorcentajeImpuesto(float porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}

	public BigDecimal getValorFinal() {
		return this.valorFinal;
	}

	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}

	public BigDecimal getValorInicial() {
		return this.valorInicial;
	}

	public void setValorInicial(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}

	public BigDecimal getValorMulta() {
		return this.valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public List<PagoLiquidacionHistorico> getPagoLiquidacionHistoricos() {
		return this.pagoLiquidacionHistoricos;
	}

	public void setPagoLiquidacionHistoricos(List<PagoLiquidacionHistorico> pagoLiquidacionHistoricos) {
		this.pagoLiquidacionHistoricos = pagoLiquidacionHistoricos;
	}

	public PagoLiquidacionHistorico addPagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().add(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setParametroImpuesto(this);

		return pagoLiquidacionHistorico;
	}

	public PagoLiquidacionHistorico removePagoLiquidacionHistorico(PagoLiquidacionHistorico pagoLiquidacionHistorico) {
		getPagoLiquidacionHistoricos().remove(pagoLiquidacionHistorico);
		pagoLiquidacionHistorico.setParametroImpuesto(null);

		return pagoLiquidacionHistorico;
	}

	public TipoServicioVehiculo getTipoServicioVehiculo() {
		return this.tipoServicioVehiculo;
	}

	public void setTipoServicioVehiculo(TipoServicioVehiculo tipoServicioVehiculo) {
		this.tipoServicioVehiculo = tipoServicioVehiculo;
	}

	public TipoVehiculo getTipoVehiculo() {
		return this.tipoVehiculo;
	}

	public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

}