package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_perfil_usuario database table.
 * 
 */
@Entity
@Table(name="tipo_perfil_usuario")
@NamedQuery(name="TipoPerfilUsuario.findAll", query="SELECT t FROM TipoPerfilUsuario t ORDER By t.idTipoPerfilUsuario")
public class TipoPerfilUsuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_PERFIL_USUARIO", unique=true, nullable=false)
	private int idTipoPerfilUsuario;

	@Column(nullable=false, length=45)
	private String perfil;

	//bi-directional many-to-many association to UsuarioAplicacion
	@ManyToMany(mappedBy="tipoPerfilUsuarios")
	private List<UsuarioAplicacion> usuarioAplicacions;

	//bi-directional many-to-one association to TipoRecursoPerfil
	@OneToMany(mappedBy="tipoPerfilUsuario")
	private List<TipoRecursoPerfil> tipoRecursoPerfils;

	public TipoPerfilUsuario() {
	}

	public int getIdTipoPerfilUsuario() {
		return this.idTipoPerfilUsuario;
	}

	public void setIdTipoPerfilUsuario(int idTipoPerfilUsuario) {
		this.idTipoPerfilUsuario = idTipoPerfilUsuario;
	}

	public String getPerfil() {
		return this.perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public List<UsuarioAplicacion> getUsuarioAplicacions() {
		return this.usuarioAplicacions;
	}

	public void setUsuarioAplicacions(List<UsuarioAplicacion> usuarioAplicacions) {
		this.usuarioAplicacions = usuarioAplicacions;
	}

	public List<TipoRecursoPerfil> getTipoRecursoPerfils() {
		return this.tipoRecursoPerfils;
	}

	public void setTipoRecursoPerfils(List<TipoRecursoPerfil> tipoRecursoPerfils) {
		this.tipoRecursoPerfils = tipoRecursoPerfils;
	}

	public TipoRecursoPerfil addTipoRecursoPerfil(TipoRecursoPerfil tipoRecursoPerfil) {
		getTipoRecursoPerfils().add(tipoRecursoPerfil);
		tipoRecursoPerfil.setTipoPerfilUsuario(this);

		return tipoRecursoPerfil;
	}

	public TipoRecursoPerfil removeTipoRecursoPerfil(TipoRecursoPerfil tipoRecursoPerfil) {
		getTipoRecursoPerfils().remove(tipoRecursoPerfil);
		tipoRecursoPerfil.setTipoPerfilUsuario(null);

		return tipoRecursoPerfil;
	}

}