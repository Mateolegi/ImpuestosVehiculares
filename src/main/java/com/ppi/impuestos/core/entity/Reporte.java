package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the reporte database table.
 * 
 */
@Entity
@Table(name="reporte")
@NamedQuery(name="Reporte.findAll", query="SELECT r FROM Reporte r")
public class Reporte implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_REPORTE", unique=true, nullable=false)
	private int idReporte;

	public Reporte() {
	}

	public int getIdReporte() {
		return this.idReporte;
	}

	public void setIdReporte(int idReporte) {
		this.idReporte = idReporte;
	}

}