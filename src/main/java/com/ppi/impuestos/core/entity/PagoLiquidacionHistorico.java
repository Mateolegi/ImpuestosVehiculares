package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the pago_liquidacion_historico database table.
 * 
 */
@Entity
@Table(name="pago_liquidacion_historico")
@NamedQuery(name="PagoLiquidacionHistorico.findAll", query="SELECT p FROM PagoLiquidacionHistorico p")
public class PagoLiquidacionHistorico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_HISTORICO", unique=true, nullable=false)
	private int idHistorico;

	@Temporal(TemporalType.DATE)
	@Column(name="FECHA_LIQUIDA", nullable=false)
	private Date fechaLiquida;

	@Column(name="PAGO_LIQUIDACION", nullable=false)
	private byte pagoLiquidacion;

	@Column(name="VALOR_TOTAL", precision=19, scale=4)
	private BigDecimal valorTotal;

	//bi-directional many-to-one association to ParametroImpuesto
	@ManyToOne
	@JoinColumn(name="ID_PARAMETRO_IMPUESTO", nullable=false)
	private ParametroImpuesto parametroImpuesto;

	//bi-directional many-to-one association to PersonaNatural
	@ManyToOne
	@JoinColumn(name="ID_PERSONA_NATURAL", nullable=false)
	private PersonaNatural personaNatural;

	//bi-directional many-to-one association to UsuarioAplicacion
	@ManyToOne
	@JoinColumn(name="ID_USUARIO", nullable=false)
	private UsuarioAplicacion usuarioAplicacion;

	//bi-directional many-to-one association to Vehiculo
	@ManyToOne
	@JoinColumn(name="ID_VEHICULO", nullable=false)
	private Vehiculo vehiculo;

	//bi-directional many-to-one association to PagoLiquidacion
	@OneToMany(mappedBy="pagoLiquidacionHistorico")
	private List<PagoLiquidacion> pagoLiquidacions;

	public PagoLiquidacionHistorico() {
	}

	public int getIdHistorico() {
		return this.idHistorico;
	}

	public void setIdHistorico(int idHistorico) {
		this.idHistorico = idHistorico;
	}

	public Date getFechaLiquida() {
		return this.fechaLiquida;
	}

	public void setFechaLiquida(Date fechaLiquida) {
		this.fechaLiquida = fechaLiquida;
	}

	public byte getPagoLiquidacion() {
		return this.pagoLiquidacion;
	}

	public void setPagoLiquidacion(byte pagoLiquidacion) {
		this.pagoLiquidacion = pagoLiquidacion;
	}

	public BigDecimal getValorTotal() {
		return this.valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public ParametroImpuesto getParametroImpuesto() {
		return this.parametroImpuesto;
	}

	public void setParametroImpuesto(ParametroImpuesto parametroImpuesto) {
		this.parametroImpuesto = parametroImpuesto;
	}

	public PersonaNatural getPersonaNatural() {
		return this.personaNatural;
	}

	public void setPersonaNatural(PersonaNatural personaNatural) {
		this.personaNatural = personaNatural;
	}

	public UsuarioAplicacion getUsuarioAplicacion() {
		return this.usuarioAplicacion;
	}

	public void setUsuarioAplicacion(UsuarioAplicacion usuarioAplicacion) {
		this.usuarioAplicacion = usuarioAplicacion;
	}

	public Vehiculo getVehiculo() {
		return this.vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public List<PagoLiquidacion> getPagoLiquidacions() {
		return this.pagoLiquidacions;
	}

	public void setPagoLiquidacions(List<PagoLiquidacion> pagoLiquidacions) {
		this.pagoLiquidacions = pagoLiquidacions;
	}

	public PagoLiquidacion addPagoLiquidacion(PagoLiquidacion pagoLiquidacion) {
		getPagoLiquidacions().add(pagoLiquidacion);
		pagoLiquidacion.setPagoLiquidacionHistorico(this);

		return pagoLiquidacion;
	}

	public PagoLiquidacion removePagoLiquidacion(PagoLiquidacion pagoLiquidacion) {
		getPagoLiquidacions().remove(pagoLiquidacion);
		pagoLiquidacion.setPagoLiquidacionHistorico(null);

		return pagoLiquidacion;
	}

}