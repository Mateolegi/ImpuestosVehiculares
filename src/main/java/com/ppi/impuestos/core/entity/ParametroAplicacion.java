package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the parametro_aplicacion database table.
 * 
 */
@Entity
@Table(name="parametro_aplicacion")
@NamedQuery(name="ParametroAplicacion.findAll", query="SELECT p FROM ParametroAplicacion p")
public class ParametroAplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PARAMETRO", unique=true, nullable=false)
	private int idParametro;

	@Lob
	private String descripcion;

	@Column(nullable=false, length=45)
	private String parametro;

	@Lob
	@Column(name="VALOR_PARAMETRO", nullable=false)
	private String valorParametro;

	public ParametroAplicacion() {
	}

	public int getIdParametro() {
		return this.idParametro;
	}

	public void setIdParametro(int idParametro) {
		this.idParametro = idParametro;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getParametro() {
		return this.parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	public String getValorParametro() {
		return this.valorParametro;
	}

	public void setValorParametro(String valorParametro) {
		this.valorParametro = valorParametro;
	}

}