package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the persona database table.
 * 
 */
@Entity
@Table(name="persona")
@NamedQueries({
	@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p ORDER BY p.idPersona"),
	@NamedQuery(name="Persona.findByNumeroDocumento", query="SELECT p FROM Persona p WHERE p.numeroDocumento = :numeroDocumento")
})
public class Persona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PERSONA", unique=true, nullable=false)
	private int idPersona;

	@Column(length=60)
	private String email;

	@Column(name="FECHA_REGISTRO", nullable=false)
	private Timestamp fechaRegistro;

	@Column(name="NUMERO_DOCUMENTO", nullable=false, length=15)
	private String numeroDocumento;

	//bi-directional many-to-one association to Moroso
	@OneToMany(mappedBy="persona")
	private List<Moroso> morosos;

	//bi-directional many-to-one association to TipoDocumento
	@ManyToOne
	@JoinColumn(name="ID_TIPO_DOCUMENTO", nullable=false)
	private TipoDocumento tipoDocumento;

	//bi-directional many-to-many association to Vehiculo
	@ManyToMany
	@JoinTable(
		name="persona_vehiculo"
		, joinColumns={
			@JoinColumn(name="ID_PERSONA", nullable=false)
			}
		, inverseJoinColumns={
			@JoinColumn(name="ID_VEHICULO", nullable=false)
			}
		)
	private List<Vehiculo> vehiculos;

	//bi-directional one-to-one association to PersonaNatural
	@OneToOne(mappedBy="persona")
	private PersonaNatural personaNatural;

	//bi-directional one-to-one association to PersonaJuridica
	@OneToOne(mappedBy="persona")
	private PersonaJuridica personaJuridica;

	public Persona() {
	}

	public int getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getFechaRegistro() {
		return this.fechaRegistro;
	}

	public void setFechaRegistro(Timestamp fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getNumeroDocumento() {
		return this.numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public List<Moroso> getMorosos() {
		return this.morosos;
	}

	public void setMorosos(List<Moroso> morosos) {
		this.morosos = morosos;
	}

	public Moroso addMoroso(Moroso moroso) {
		getMorosos().add(moroso);
		moroso.setPersona(this);

		return moroso;
	}

	public Moroso removeMoroso(Moroso moroso) {
		getMorosos().remove(moroso);
		moroso.setPersona(null);

		return moroso;
	}

	public TipoDocumento getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public List<Vehiculo> getVehiculos() {
		return this.vehiculos;
	}

	public void setVehiculos(List<Vehiculo> vehiculos) {
		this.vehiculos = vehiculos;
	}

	public PersonaNatural getPersonaNatural() {
		return this.personaNatural;
	}

	public void setPersonaNatural(PersonaNatural personaNatural) {
		this.personaNatural = personaNatural;
	}

	public PersonaJuridica getPersonaJuridica() {
		return this.personaJuridica;
	}

	public void setPersonaJuridica(PersonaJuridica personaJuridica) {
		this.personaJuridica = personaJuridica;
	}

}