package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tipo_recurso_perfil database table.
 * 
 */
@Entity
@Table(name="tipo_recurso_perfil")
@NamedQuery(name="TipoRecursoPerfil.findAll", query="SELECT t FROM TipoRecursoPerfil t")
public class TipoRecursoPerfil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_TIPO_RECURSO_PERFIL", unique=true, nullable=false)
	private int idTipoRecursoPerfil;

	@Column(nullable=false, length=45)
	private String recurso;

	//bi-directional many-to-many association to UsuarioAplicacion
	@ManyToMany(mappedBy="tipoRecursoPerfils")
	private List<UsuarioAplicacion> usuarioAplicacions;

	//bi-directional many-to-one association to TipoPerfilUsuario
	@ManyToOne
	@JoinColumn(name="ID_PERFIL", nullable=false)
	private TipoPerfilUsuario tipoPerfilUsuario;

	public TipoRecursoPerfil() {
	}

	public int getIdTipoRecursoPerfil() {
		return this.idTipoRecursoPerfil;
	}

	public void setIdTipoRecursoPerfil(int idTipoRecursoPerfil) {
		this.idTipoRecursoPerfil = idTipoRecursoPerfil;
	}

	public String getRecurso() {
		return this.recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public List<UsuarioAplicacion> getUsuarioAplicacions() {
		return this.usuarioAplicacions;
	}

	public void setUsuarioAplicacions(List<UsuarioAplicacion> usuarioAplicacions) {
		this.usuarioAplicacions = usuarioAplicacions;
	}

	public TipoPerfilUsuario getTipoPerfilUsuario() {
		return this.tipoPerfilUsuario;
	}

	public void setTipoPerfilUsuario(TipoPerfilUsuario tipoPerfilUsuario) {
		this.tipoPerfilUsuario = tipoPerfilUsuario;
	}

}