package com.ppi.impuestos.core.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the semaforizacion database table.
 * 
 */
@Entity
@Table(name="semaforizacion")
@NamedQueries({
	@NamedQuery(name="Semaforizacion.findAll", query="SELECT s FROM Semaforizacion s ORDER BY s.ano ASC"),
	@NamedQuery(name="Semaforizacion.findByYear", query="SELECT s FROM Semaforizacion s WHERE s.ano = :ano")
})
public class Semaforizacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_SEMAFORIZACION", unique=true, nullable=false)
	private int idSemaforizacion;

	@Temporal(TemporalType.DATE)
	@Column(nullable=false)
	private Date ano;

	@Column(nullable=false, precision=10, scale=4)
	private BigDecimal valor;

	public Semaforizacion() {
	}

	public int getIdSemaforizacion() {
		return this.idSemaforizacion;
	}

	public void setIdSemaforizacion(int idSemaforizacion) {
		this.idSemaforizacion = idSemaforizacion;
	}

	public Date getAno() {
		return this.ano;
	}

	public void setAno(Date ano) {
		this.ano = ano;
	}

	public BigDecimal getValor() {
		return this.valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

}