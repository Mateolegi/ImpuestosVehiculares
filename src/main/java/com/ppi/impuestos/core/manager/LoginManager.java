package com.ppi.impuestos.core.manager;

import java.util.Date;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.Severity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.dto.LoginDTO;
import com.ppi.impuestos.core.entity.UsuarioAplicacion;
import com.ppi.impuestos.core.exception.entity.ErrorMessage;
import com.ppi.impuestos.core.util.KeyUtils;
import com.ppi.impuestos.gerencia.dao.UsuarioAplicacionDAO;
import com.ppi.impuestos.gerencia.dto.mapper.UsuarioAplicacionMapper;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class LoginManager {

	private static final Logger LOGGER = LogManager.getLogger(LoginManager.class);
	private Gson gson;
	private ErrorMessage errorMessage = new ErrorMessage();
	
	public LoginManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	public Response login(LoginDTO dto, UriInfo uriInfo) {
		LOGGER.info("Intento de inicio de sesión con el usuario " + dto.getUsuario());
		UsuarioAplicacion user = validarCredenciales(dto);
		if(user != null) {
			String jwtToken = generateToken(user.getIdUsuario(), user.getNombreUsuario(), uriInfo.getAbsolutePath().toString());
			return Response.ok()
					.header(HttpHeaders.AUTHORIZATION, "Bearer " + jwtToken)
					.entity(gson.toJson(UsuarioAplicacionMapper.toDTO(user)))
					.build();
		} else {
			errorMessage.setErrorCode(Response.Status.UNAUTHORIZED);
			errorMessage.setErrorMessage("Nombre de usuario y/o contraseña incorrectas.");
			errorMessage.setSeverity(Severity.WARNING);
			return Response.status(Response.Status.UNAUTHORIZED).entity(errorMessage).build();
		}
	}
	
	public UsuarioAplicacion validarCredenciales(LoginDTO dto) {
		UsuarioAplicacionDAO dao = new UsuarioAplicacionDAO();
		boolean isEmail = EmailValidator.getInstance(false).isValid(dto.getUsuario());
		UsuarioAplicacion usuarioAplicacion;
		if(isEmail) {
			usuarioAplicacion = dao.findByEmail(dto.getUsuario());
		} else {
			usuarioAplicacion = dao.findByUsername(dto.getUsuario());
		}
		if(usuarioAplicacion != null) {
			String pass = usuarioAplicacion.getContrasena();
			return pass != null && pass.equals(dto.getContrasena()) ? usuarioAplicacion : null;
		}
		return null;
	}

	public String generateToken(int id, String user, String uri) {
		try {
			String jwtToken = Jwts.builder()
					.setId(String.valueOf(id))
					.setSubject(user)
					.setIssuer(uri)
					.setIssuedAt(new Date())
//					.setExpiration(Date.from(LocalDateTime.now().plusMinutes(15L).atZone(ZoneId.systemDefault()).toInstant()))
					.signWith(SignatureAlgorithm.HS512, KeyUtils.KEY)
					.compact();
			LOGGER.info("Se generó un token para la llave: " + jwtToken + " - " + KeyUtils.KEY);
			return jwtToken;
		} catch (Exception e) {
			errorMessage.setErrorCode(Response.Status.INTERNAL_SERVER_ERROR);
			errorMessage.setErrorMessage("Ocurrió un error generando el token");
			errorMessage.setSeverity(Severity.FATAL);
			LOGGER.error("Ocurrió un error generando el token", e);
			return null;
		}
	}
	
	public Claims parseJWT(String jwt) {
	    //This line will throw an exception if it is not a signed JWS (as expected)
	    return Jwts.parser()
	    		.setSigningKey(KeyUtils.KEY)
	    		.parseClaimsJws(jwt).getBody();
	}
}
