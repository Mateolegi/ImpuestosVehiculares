package com.ppi.impuestos.core.manager;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import javassist.NotFoundException;

public interface CrudManager {

	/**
	 * Obtiene una lista de todas las entidades
	 * @return lista con todas las entidades
	 */
	public String findAll(UriInfo uriInfo);

	/**
	 * Obtiene una entidad por su id
	 * @param id Id de la entidad
	 * @return entidad encontrada
	 */
	public String find(int id) throws NotFoundException;

	/**
	 * Almacena una nueva entidad
	 * @param entity
	 * @return
	 */
//	public Response save(DTO entity, UriInfo uriInfo);

	/**
	 * Actualiza una entidad
	 * @param entity entidad a actualizar
	 * @return entidad actualizada
	 */
//	public String update(int id, DTO entity);

	/**
	 * Elimina una entidad
	 * @param id id de la entidad
	 */
	public Response delete(int id);
}
