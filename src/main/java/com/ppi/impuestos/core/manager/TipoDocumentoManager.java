package com.ppi.impuestos.core.manager;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.dao.TipoDocumentoDAO;
import com.ppi.impuestos.core.entity.TipoDocumento;
import com.ppi.impuestos.gerencia.dto.mapper.TipoDocumentoMapper;

import javassist.NotFoundException;

public class TipoDocumentoManager implements CrudManager {

	private TipoDocumentoDAO dao = new TipoDocumentoDAO();
	private Gson gson;

	public TipoDocumentoManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	@Override
	public String findAll(UriInfo uriInfo) {
		return gson.toJson(TipoDocumentoMapper.toDTOList(dao.findAll()));
	}

	@Override
	public String find(int id) throws NotFoundException {
		TipoDocumento tipoDocumento = dao.find(id);
		if(tipoDocumento == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(TipoDocumentoMapper.toDTO(tipoDocumento));
	}

	@Override
	public Response delete(int id) {
		dao.delete(dao.find(id));
		if(dao.find(id) == null) return Response.noContent().build();
		return Response.serverError().build();
	}

}
