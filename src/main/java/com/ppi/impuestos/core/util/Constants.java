package com.ppi.impuestos.core.util;

public class Constants {

	public static final String DATE_FORMAT = "yyyy-MM-dd";

	private Constants() { }
}
