package com.ppi.impuestos.core.util;

import static com.ppi.impuestos.core.util.KeyUtils.*;

import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AES256 {

	private static final Logger LOGGER = LogManager.getLogger(AES256.class);
	
	private AES256() { }

	public static String encrypt(String text) {
		try {
			byte[] ivBytes;
			SecureRandom random = new SecureRandom();
			byte[] bytes = new byte[20];
			random.nextBytes(bytes);
			byte[] saltBytes = bytes;
			// Derive the key
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			PBEKeySpec spec = new PBEKeySpec(PRIVATE_KEY.toCharArray(), saltBytes, 65556, 256);
			SecretKey secretKey = factory.generateSecret(spec);
			SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
			// encrypting the word
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secret);
			AlgorithmParameters params = cipher.getParameters();
			ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
			byte[] encryptedTextBytes = cipher.doFinal(text.getBytes("UTF-8"));
			// prepend salt and vi
			byte[] buffer = new byte[saltBytes.length + ivBytes.length + encryptedTextBytes.length];
			System.arraycopy(saltBytes, 0, buffer, 0, saltBytes.length);
			System.arraycopy(ivBytes, 0, buffer, saltBytes.length, ivBytes.length);
			System.arraycopy(encryptedTextBytes, 0, buffer, saltBytes.length + ivBytes.length,
					encryptedTextBytes.length);
			return new Base64().encodeToString(buffer);
		} catch (Exception e) {
			LOGGER.error("Ocurrió un error cifrando el texto", e);
			return null;
		}
	}

	public static String decrypt(String encryptedText) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		    //strip off the salt and iv
		    ByteBuffer buffer = ByteBuffer.wrap(new Base64().decode(encryptedText));
		    byte[] saltBytes = new byte[20];
		    buffer.get(saltBytes, 0, saltBytes.length);
		    byte[] ivBytes1 = new byte[cipher.getBlockSize()];
		    buffer.get(ivBytes1, 0, ivBytes1.length);
		    byte[] encryptedTextBytes = new byte[buffer.capacity() - saltBytes.length - ivBytes1.length];
		    buffer.get(encryptedTextBytes);
		    // Deriving the key
		    SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		    PBEKeySpec spec = new PBEKeySpec(PRIVATE_KEY.toCharArray(), saltBytes, 65556, 256);
		    SecretKey secretKey = factory.generateSecret(spec);
		    SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
		    cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes1));
		    byte[] decryptedTextBytes = null;
		    decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		    return new String(decryptedTextBytes);
		} catch (Exception e) {
			LOGGER.error("No se pudo descifrar el texto", e);
			return null;
		}
	}

	public static String getSalt() throws NoSuchAlgorithmException {
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		byte[] salt = new byte[20];
		sr.nextBytes(salt);
		return new String(salt);
	}
}