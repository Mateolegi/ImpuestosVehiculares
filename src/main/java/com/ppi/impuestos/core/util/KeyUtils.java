package com.ppi.impuestos.core.util;

import java.security.Key;

import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.SignatureAlgorithm;

public class KeyUtils {
	
	public static final String PUBLIC_KEY = "";
	public static final String PRIVATE_KEY = "MlTC7nYf2dPOBFgYAVqMwuRAYwkru5V5";
	public static final Key KEY = new SecretKeySpec(PRIVATE_KEY.getBytes(), SignatureAlgorithm.HS512.getJcaName());
	
	private KeyUtils() { }
}
