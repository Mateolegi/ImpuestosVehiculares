package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.manager.TipoServicioVehiculoManager;

import javassist.NotFoundException;

@RequestScoped
@Path("tipo-servicio-vehiculos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TipoServicioVehiculoService {

	@Context UriInfo uriInfo;
	TipoServicioVehiculoManager manager = new TipoServicioVehiculoManager();
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}
	
	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public String find(@PathParam("id") int id) throws NotFoundException {
		return manager.find(id);
	}
}
