package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.registro.dto.VehiculoDTO;
import com.ppi.impuestos.registro.manager.VehiculoManager;

import javassist.NotFoundException;

@RequestScoped
@Path("vehiculos")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class VehiculoService {

	private VehiculoManager manager = new VehiculoManager();
	
	@Context
	private UriInfo uriInfo;
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}
	
	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public String find(@PathParam("id") int id) throws NotFoundException {
		return manager.find(id);
	}
	
	@POST
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public Response save(VehiculoDTO dto) {
		return manager.save(dto, uriInfo);
	}
	
	@PUT
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public Response update(@PathParam("id") int id, VehiculoDTO usuarioAplicacion) {
		return manager.update(id, usuarioAplicacion);
	}
	
	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Vehículos"})
	public Response delete(@PathParam("id") int id) {
		return manager.delete(id);
	}
}
