package com.ppi.impuestos.core.service;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.core.manager.TipoDocumentoManager;

import javassist.NotFoundException;

@RequestScoped
@Path("tipo-documentos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TipoDocumentoService {

	@Context UriInfo uriInfo;
	private TipoDocumentoManager manager = new TipoDocumentoManager();
	
	@GET
	@JWTTokenNeeded
	public String findAll() {
		return manager.findAll(uriInfo);
	}
	
	@GET
	@Path("{id}")
	@JWTTokenNeeded
	public String find(@PathParam("id") int id) throws NotFoundException {
		return manager.find(id);
	}
}
