package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.dto.UsuarioAplicacionDTO;
import com.ppi.impuestos.gerencia.manager.UsuarioAplicacionManager;

@RequestScoped
@Path("usuarios")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioAplicacionService {

	private UsuarioAplicacionManager manager = new UsuarioAplicacionManager();
	
	@Context
	private UriInfo uriInfo;
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}
	
	@GET
	@Path("{id}")
	@JWTTokenNeeded
	public String find(@PathParam("id") int id) {
		return manager.find(id);
	}
	
	@POST
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response save(UsuarioAplicacionDTO dto) throws Exception {
		return manager.save(dto, uriInfo);
	}
	
	@PUT
	@Path("{id}")
	@JWTTokenNeeded
	public String update(@PathParam("id") int id, UsuarioAplicacionDTO usuarioAplicacion) throws Exception {
		return manager.update(id, usuarioAplicacion);
	}
	
	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response delete(@PathParam("id") int id) {
		return manager.delete(id);
	}
}
