package com.ppi.impuestos.core.service;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.dto.LoginDTO;
import com.ppi.impuestos.core.manager.LoginManager;

@RequestScoped
@Path("login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginService {
	
	LoginManager manager = new LoginManager();

	@Context private UriInfo uriInfo;
	@Context HttpHeaders headers;
	
	@POST
	public Response login(LoginDTO dto) {
		return manager.login(dto, uriInfo);
	}
}
