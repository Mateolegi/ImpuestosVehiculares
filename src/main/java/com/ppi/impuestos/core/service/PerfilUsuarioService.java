package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.dto.TipoPerfilUsuarioDTO;
import com.ppi.impuestos.gerencia.manager.PerfilUsuarioManager;

@RequestScoped
@Path("perfiles")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PerfilUsuarioService {

	private PerfilUsuarioManager manager =  new PerfilUsuarioManager();
	
	@Context
	private UriInfo uriInfo;
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}
	
	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String find(@PathParam("id") int id) {
		return manager.find(id);
	}
	
	@POST
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response save(TipoPerfilUsuarioDTO perfilUsuario) {
		return manager.save(perfilUsuario, uriInfo);
	}
	
	@PUT
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String update(@PathParam("id") int id, TipoPerfilUsuarioDTO perfilUsuario) {
		return manager.update(id, perfilUsuario);
	}
	
	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response delete(@PathParam("id") int id) {
		return manager.delete(id);
	}

	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	@Path("{perfilId}/recursos")
	public RecursoPerfilService getRecursoPerfilService() {
		return new RecursoPerfilService();
	}
}
