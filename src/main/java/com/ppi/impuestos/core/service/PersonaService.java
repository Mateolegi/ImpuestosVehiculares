package com.ppi.impuestos.core.service;

import java.text.ParseException;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.dto.PersonaNaturalDTO;
import com.ppi.impuestos.gerencia.manager.PersonaManager;

import javassist.NotFoundException;

@RequestScoped
@Path("personas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonaService {

	@Context private UriInfo uriInfo;
	private PersonaManager manager = new PersonaManager();
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}

	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
	public String find(@PathParam("id")int id) throws NotFoundException {
		return manager.find(id);
	}

	@POST
	@Path("natural")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
	public String save(PersonaNaturalDTO dto) throws ParseException {
		return manager.savePersonaNatural(dto);
	}

//	@POST
//	@JWTTokenNeeded
//	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
//	public Response save(PersonaDTO impuesto, @Context UriInfo uriInfo)  {
//		return  manager.save(impuesto, uriInfo);
//	}

//	@PUT
//	@Path("{id}")
//	@JWTTokenNeeded
//	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
//	public String update(@PathParam("id") int id, PersonaDTO dto) {
//		return manager.update(id, dto);
//	}

	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios", "Propietarios", "Vehículos", "Liquidación", "Pagos"})
	public Response delete(@PathParam ("id") int id) {
		return manager.delete(id);
	}
}
