package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.dto.ParametroImpuestoDTO;
import com.ppi.impuestos.gerencia.manager.ParametroImpuestoManager;

import javassist.NotFoundException;

@RequestScoped
@Path("parametro-impuestos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ParametroImpuestoService {
	
	@Context private UriInfo uriInfo;
	private ParametroImpuestoManager manager = new ParametroImpuestoManager();
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Parametrización"})
	public String findAll() {
		return manager.findAll(uriInfo);
	}

	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Parametrización"})
	public String find(@PathParam("id")int id) throws NotFoundException {
		return manager.find(id);
	}

	@POST
	@JWTTokenNeeded
	@RolesAllowed({"Parametrización"})
	public Response save(ParametroImpuestoDTO impuesto, @Context UriInfo uriInfo)  {
		return  manager.save(impuesto, uriInfo);
	}

	@PUT
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Parametrización"})
	public String update(@PathParam("id") int id, ParametroImpuestoDTO parametro) {
		return manager.update(id, parametro);
	}

	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Parametrización"})
	public Response delete(@PathParam ("id") int id) {
		return manager.delete(id);
	}
}
