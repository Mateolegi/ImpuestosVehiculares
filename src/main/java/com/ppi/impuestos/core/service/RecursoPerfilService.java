package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.gerencia.dto.TipoRecursoPerfilDTO;
import com.ppi.impuestos.gerencia.manager.RecursoPerfilManager;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RecursoPerfilService {

	@Context UriInfo uriInfo;

	private RecursoPerfilManager manager = new RecursoPerfilManager();

	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String findAll(@PathParam("perfilId") int perfilId) {
		return manager.findAll(perfilId);
	}

	@GET
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String find(@PathParam("perfilId") int perfilId, @PathParam("id") int id) {
		return manager.find(perfilId, id);
	}

	@POST
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response save(@PathParam("perfilId") int perfilId, TipoRecursoPerfilDTO recursoPerfil) {
		return manager.save(perfilId, recursoPerfil, uriInfo);
	}

	@PUT
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public String update(@PathParam("perfilId") int perfilId, @PathParam("id") int id,
			TipoRecursoPerfilDTO recursoPerfil) {
		return manager.update(perfilId, id, recursoPerfil);
	}

	@DELETE
	@Path("{id}")
	@JWTTokenNeeded
	@RolesAllowed({"Usuarios"})
	public Response delete(@PathParam("perfilId") int perfilId, @PathParam("id") int id) {
		return manager.delete(perfilId, id);
	}
}
