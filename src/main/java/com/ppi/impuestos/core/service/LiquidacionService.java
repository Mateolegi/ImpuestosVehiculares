package com.ppi.impuestos.core.service;

import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import com.ppi.impuestos.core.annotation.JWTTokenNeeded;
import com.ppi.impuestos.liquidacion.manager.LiquidacionManager;

@RequestScoped
@Path("liquidaciones")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LiquidacionService {

	@Context private UriInfo uriInfo;
	private LiquidacionManager manager = new LiquidacionManager();
	
	@GET
	@JWTTokenNeeded
	@RolesAllowed({"Liquidación", "Pagos"})
	public String findAll(@HeaderParam("Authorization") String authorizationToken) {
		return manager.findAll(uriInfo, authorizationToken);
	}
}
