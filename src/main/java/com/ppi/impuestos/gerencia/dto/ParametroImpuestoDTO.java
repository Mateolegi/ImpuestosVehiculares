package com.ppi.impuestos.gerencia.dto;

import java.math.BigDecimal;

import com.ppi.impuestos.core.dto.DTO;

public class ParametroImpuestoDTO implements DTO {

	private int idParametroImpuesto; 
	private TipoVehiculoDTO tipoVehiculo;
	private TipoServicioVehiculoDTO tipoServicioVehiculo;
	private BigDecimal valorInicial;
	private BigDecimal valorFinal;
	private float porcentajeImpuesto;
	private float porcentajeDescuento;
	private BigDecimal valorMulta;
	
	public int getIdParametroImpuesto() {
		return idParametroImpuesto;
	}
	public void setIdParametroImpuesto(int idParametroImpuesto) {
		this.idParametroImpuesto = idParametroImpuesto;
	}
	public TipoVehiculoDTO getTipoVehiculo() {
		return tipoVehiculo;
	}
	public void setTipoVehiculo(TipoVehiculoDTO tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}
	public TipoServicioVehiculoDTO getTipoServicioVehiculo() {
		return tipoServicioVehiculo;
	}
	public void setTipoServicioVehiculo(TipoServicioVehiculoDTO tipoServicioVehiculo) {
		this.tipoServicioVehiculo = tipoServicioVehiculo;
	}
	public BigDecimal getValorInicial() {
		return valorInicial;
	}
	public void setValorInicial(BigDecimal valorInicial) {
		this.valorInicial = valorInicial;
	}
	public BigDecimal getValorFinal() {
		return valorFinal;
	}
	public void setValorFinal(BigDecimal valorFinal) {
		this.valorFinal = valorFinal;
	}
	public float getPorcentajeImpuesto() {
		return porcentajeImpuesto;
	}
	public void setPorcentajeImpuesto(float porcentajeImpuesto) {
		this.porcentajeImpuesto = porcentajeImpuesto;
	}
	public float getPorcentajeDescuento() {
		return porcentajeDescuento;
	}
	public void setPorcentajeDescuento(float porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	}
	public BigDecimal getValorMulta() {
		return valorMulta;
	}
	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}
}
