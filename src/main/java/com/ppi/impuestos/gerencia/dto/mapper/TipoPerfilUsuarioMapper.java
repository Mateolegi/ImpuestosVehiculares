package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.TipoPerfilUsuario;
import com.ppi.impuestos.gerencia.dto.TipoPerfilUsuarioDTO;

public class TipoPerfilUsuarioMapper {
	
	private TipoPerfilUsuarioMapper() { }
	
	public static TipoPerfilUsuario toEntity(TipoPerfilUsuarioDTO dto) {
		TipoPerfilUsuario entity = new TipoPerfilUsuario();
		if(validateId(dto.getIdTipoPerfilUsuario())) entity.setIdTipoPerfilUsuario(dto.getIdTipoPerfilUsuario());
		if(validatePerfil(dto.getPerfil())) entity.setPerfil(dto.getPerfil());
		return entity;
	}
	
	private static boolean validateId(Integer id) {
		return id != null && id > 0;
	}
	
	private static boolean validatePerfil(String perfil) {
		return perfil != null && perfil.trim().length() >= 5 && perfil.trim().length() <= 45;
	}
	
	public static TipoPerfilUsuarioDTO toDTO(TipoPerfilUsuario entity) {
		TipoPerfilUsuarioDTO dto = new TipoPerfilUsuarioDTO();
		dto.setIdTipoPerfilUsuario(entity.getIdTipoPerfilUsuario());
		dto.setPerfil(entity.getPerfil());
		return dto;
	}
	
	public static List<TipoPerfilUsuarioDTO> toDTOList(List<TipoPerfilUsuario> entities) {
		List<TipoPerfilUsuarioDTO> list = new ArrayList<>();
		for(TipoPerfilUsuario entity: entities) list.add(toDTO(entity));
		return list;
	}
	
	public static List<TipoPerfilUsuario> toEntityList(List<TipoPerfilUsuarioDTO> dtos) {
		List<TipoPerfilUsuario> list = new ArrayList<>();
		for(TipoPerfilUsuarioDTO dto: dtos) list.add(toEntity(dto));
		return list;
	}
}
