package com.ppi.impuestos.gerencia.dto.mapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.ppi.impuestos.core.entity.PersonaNatural;
import com.ppi.impuestos.core.util.Constants;
import com.ppi.impuestos.gerencia.dao.PersonaDAO;
import com.ppi.impuestos.gerencia.dto.PersonaNaturalDTO;

public class PersonaNaturalMapper {
	
	private PersonaNaturalMapper() { }

	public static PersonaNaturalDTO toDTO(PersonaNatural entity) {
		final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		final PersonaNaturalDTO dto = new PersonaNaturalDTO();
		dto.setIdPersonaNatural(entity.getIdPersonaNatural());
		dto.setPrimerNombre(entity.getPrimerNombre());
		dto.setSegundoNombre(entity.getSegundoNombre());
		dto.setPrimerApellido(entity.getPrimerApellido());
		dto.setSegundoApellido(entity.getSegundoApellido());
		dto.setSexo(entity.getSexo());
		dto.setTelefonoMovil(entity.getTelefonoMovil());
		dto.setPersona(PersonaMapper.toDTO(entity.getPersona()));
		dto.setFechaNacimiento(formatter.format(entity.getFechaNacimiento()));
		return dto;
	}
	
	public static PersonaNatural toEntity(PersonaNaturalDTO dto) throws ParseException {
		final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		final PersonaNatural entity = new PersonaNatural();
		entity.setFechaNacimiento(formatter.parse(dto.getFechaNacimiento()));
		entity.setPersona(PersonaMapper.toEntity(dto.getPersona()));
		entity.setPrimerApellido(dto.getPrimerApellido());
		entity.setPrimerNombre(dto.getPrimerNombre());
		entity.setSegundoApellido(dto.getSegundoApellido());
		entity.setSegundoNombre(dto.getSegundoNombre());
		entity.setSexo(dto.getSexo());
		entity.setTelefonoMovil(dto.getTelefonoMovil());
		return entity;
	}

	public static PersonaNatural update(PersonaNatural entity, PersonaNaturalDTO dto) throws ParseException {
		final DateFormat formatter = new SimpleDateFormat(Constants.DATE_FORMAT);
		if(dto.getFechaNacimiento() != null) entity.setFechaNacimiento(formatter.parse(dto.getFechaNacimiento()));
		entity.setPersona(PersonaMapper.update(new PersonaDAO().find(dto.getPersona().getIdPersona()), dto.getPersona()));
		if(dto.getPrimerApellido() != null) entity.setPrimerApellido(dto.getPrimerApellido());
		if(dto.getPrimerNombre() != null) entity.setPrimerNombre(dto.getPrimerNombre());
		entity.setSegundoApellido(dto.getSegundoApellido());
		entity.setSegundoNombre(dto.getSegundoNombre());
		if(dto.getSexo() != null) entity.setSexo(dto.getSexo());
		entity.setTelefonoMovil(dto.getTelefonoMovil());
		return entity;
	}
}
