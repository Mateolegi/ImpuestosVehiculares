package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.TipoRecursoPerfil;
import com.ppi.impuestos.gerencia.dto.TipoRecursoPerfilDTO;

public class TipoRecursoPerfilMapper {
	
	private TipoRecursoPerfilMapper() { }

	public static TipoRecursoPerfilDTO toDTO(TipoRecursoPerfil entity) {
		TipoRecursoPerfilDTO dto = new TipoRecursoPerfilDTO();
		dto.setIdTipoRecursoPerfil(entity.getIdTipoRecursoPerfil());
		dto.setRecurso(entity.getRecurso());
		dto.setPerfil(TipoPerfilUsuarioMapper.toDTO(entity.getTipoPerfilUsuario()));
		return dto;
	}
	
	public static List<TipoRecursoPerfilDTO> toDTOList(List<TipoRecursoPerfil> entities) {
		List<TipoRecursoPerfilDTO> list = new ArrayList<>();
		for(TipoRecursoPerfil entity: entities) list.add(toDTO(entity));
		return list;
	}

	public static List<TipoRecursoPerfil> toEntityList(List<TipoRecursoPerfilDTO> dtos) {
		List<TipoRecursoPerfil> list = new ArrayList<>();
		for(TipoRecursoPerfilDTO dto: dtos) list.add(toEntity(dto));
		return list;
	}

	public static TipoRecursoPerfil toEntity(TipoRecursoPerfilDTO dto) {
		TipoRecursoPerfil entity = new TipoRecursoPerfil();
		entity.setIdTipoRecursoPerfil(dto.getIdTipoRecursoPerfil());
		entity.setRecurso(dto.getRecurso());
		entity.setTipoPerfilUsuario(TipoPerfilUsuarioMapper.toEntity(dto.getPerfil()));
		return entity;
	}
}
