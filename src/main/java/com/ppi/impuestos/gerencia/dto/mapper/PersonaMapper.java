package com.ppi.impuestos.gerencia.dto.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.Persona;
import com.ppi.impuestos.gerencia.dto.PersonaDTO;

public class PersonaMapper {
	
	private PersonaMapper() { }

	public static PersonaDTO toDTO(Persona entity) {
		final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		PersonaDTO dto = new PersonaDTO();
		dto.setIdPersona(entity.getIdPersona());
		dto.setTipoDocumento(TipoDocumentoMapper.toDTO(entity.getTipoDocumento()));
		dto.setNumeroDocumento(entity.getNumeroDocumento());
		dto.setEmail(entity.getEmail());
		dto.setFechaRegistro(formatter.format(entity.getFechaRegistro()));
		return dto;
	}

	public static List<PersonaDTO> toDTOList(List<Persona> entities) {
		List<PersonaDTO> list = new ArrayList<>();
		for(Persona entity: entities) list.add(toDTO(entity));
		return list;
	}

	public static Persona toEntity(PersonaDTO dto) {
		Persona entity = new Persona();
		entity.setEmail(dto.getEmail());
		entity.setNumeroDocumento(dto.getNumeroDocumento());
		entity.setTipoDocumento(TipoDocumentoMapper.toEntity(dto.getTipoDocumento()));
		return entity;
	}

	public static Persona update(Persona entity, PersonaDTO dto) {
		if(dto.getEmail() != null) entity.setEmail(dto.getEmail());
		return entity;
	}
}
