package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.TipoVehiculo;
import com.ppi.impuestos.gerencia.dto.TipoVehiculoDTO;

public class TipoVehiculoMapper {
	
	private TipoVehiculoMapper() { }

	public static TipoVehiculoDTO toDTO(TipoVehiculo entity) {
		TipoVehiculoDTO dto = new TipoVehiculoDTO();
		dto.setIdTipoVehiculo(entity.getIdTipoVehiculo());
		dto.setTipoVehiculo(entity.getTipoVehiculo());
		return dto;
	}
	
	public static List<TipoVehiculoDTO> toDTOList(List<TipoVehiculo> entities) {
		List<TipoVehiculoDTO> list = new ArrayList<>();
		for(TipoVehiculo entity: entities) list.add(toDTO(entity));
		return list;
	}
}
