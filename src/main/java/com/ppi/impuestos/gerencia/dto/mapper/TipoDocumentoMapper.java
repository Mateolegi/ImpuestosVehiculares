package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.TipoDocumento;
import com.ppi.impuestos.gerencia.dto.TipoDocumentoDTO;

public class TipoDocumentoMapper {
	
	private TipoDocumentoMapper() { }

	public static TipoDocumentoDTO toDTO(TipoDocumento entity) {
		TipoDocumentoDTO dto = new TipoDocumentoDTO();
		dto.setIdTipoDocumento(entity.getIdTipoDocumento());
		dto.setTipoDocumento(entity.getTipoDocumento());
		dto.setAbreviacion(entity.getAbreviacion());
		return dto;
	}
	
	public static TipoDocumento toEntity(TipoDocumentoDTO dto) {
		TipoDocumento entity = new TipoDocumento();
		entity.setIdTipoDocumento(dto.getIdTipoDocumento());
		entity.setTipoDocumento(dto.getTipoDocumento());
		entity.setAbreviacion(dto.getAbreviacion());
		return entity;
	}

	public static List<TipoDocumentoDTO> toDTOList(List<TipoDocumento> entities) {
		List<TipoDocumentoDTO> list = new ArrayList<>();
		for(TipoDocumento entity: entities) list.add(toDTO(entity));
		return list;
	}
}
