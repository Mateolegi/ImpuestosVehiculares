package com.ppi.impuestos.gerencia.dto;

import com.ppi.impuestos.core.dto.DTO;

public class PersonaJuridicaDTO implements DTO {

	private int idPersonaJuridica;
	private String razonSocial;
	private PersonaDTO persona;

	public int getIdPersonaJuridica() {
		return idPersonaJuridica;
	}
	public void setIdPersonaJuridica(int idPersonaJuridica) {
		this.idPersonaJuridica = idPersonaJuridica;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public PersonaDTO getPersona() {
		return persona;
	}
	public void setPersona(PersonaDTO persona) {
		this.persona = persona;
	}
}
