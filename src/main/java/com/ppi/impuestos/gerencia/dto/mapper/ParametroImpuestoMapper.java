package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.dao.TipoServicioVehiculoDAO;
import com.ppi.impuestos.core.dao.TipoVehiculoDAO;
import com.ppi.impuestos.core.entity.ParametroImpuesto;
import com.ppi.impuestos.gerencia.dto.ParametroImpuestoDTO;

public class ParametroImpuestoMapper {
	
	private ParametroImpuestoMapper() { }
	
	public static ParametroImpuestoDTO toDTO(ParametroImpuesto entity) {
		ParametroImpuestoDTO dto = new ParametroImpuestoDTO();
		dto.setIdParametroImpuesto(entity.getIdParametroImpuesto());
		dto.setTipoVehiculo(TipoVehiculoMapper.toDTO(entity.getTipoVehiculo()));
		dto.setTipoServicioVehiculo(TipoServicioVehiculoMapper.toDTO(entity.getTipoServicioVehiculo()));
		dto.setValorInicial(entity.getValorInicial());
		dto.setValorFinal(entity.getValorFinal());
		dto.setPorcentajeImpuesto(entity.getPorcentajeImpuesto());
		dto.setPorcentajeDescuento(entity.getPorcentajeDescuento());
		dto.setValorMulta(entity.getValorMulta());
		return dto;
	}
	
	public static ParametroImpuesto toEntity(ParametroImpuestoDTO dto) {
		ParametroImpuesto entity = new ParametroImpuesto ();
		entity.setTipoVehiculo(new TipoVehiculoDAO().find(dto.getTipoVehiculo().getIdTipoVehiculo()));
		entity.setTipoServicioVehiculo(new TipoServicioVehiculoDAO()
				.find(dto.getTipoServicioVehiculo().getIdTipoServicioVehiculo()));
		entity.setPorcentajeDescuento(dto.getPorcentajeDescuento());
		entity.setPorcentajeImpuesto(dto.getPorcentajeImpuesto());
		entity.setValorFinal(dto.getValorFinal());
		entity.setValorInicial(dto.getValorInicial());
		entity.setValorMulta(dto.getValorMulta());
		return entity;
	}
	
	public static List<ParametroImpuestoDTO> toDTOList(List<ParametroImpuesto> entities) {
		List<ParametroImpuestoDTO> list = new ArrayList<>();
		for(ParametroImpuesto entity: entities) list.add(toDTO(entity));
		return list;
	}
}
