package com.ppi.impuestos.gerencia.dto.mapper;

import com.ppi.impuestos.core.entity.PersonaJuridica;
import com.ppi.impuestos.gerencia.dto.PersonaJuridicaDTO;

public class PersonaJuridicaMapper {

	private PersonaJuridicaMapper() { }

	public static PersonaJuridicaDTO toDTO(PersonaJuridica entity) {
		PersonaJuridicaDTO dto = new PersonaJuridicaDTO();
		dto.setIdPersonaJuridica(entity.getIdPersonaJuridica());
		dto.setRazonSocial(entity.getRazonSocial());
		dto.setPersona(PersonaMapper.toDTO(entity.getPersona()));
		return dto;
	}
}
