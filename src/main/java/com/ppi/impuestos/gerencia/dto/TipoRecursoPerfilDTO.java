package com.ppi.impuestos.gerencia.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TipoRecursoPerfilDTO {

	private int idTipoRecursoPerfil;
	private String recurso;
	private TipoPerfilUsuarioDTO perfil;
	
	public int getIdTipoRecursoPerfil() {
		return idTipoRecursoPerfil;
	}
	public void setIdTipoRecursoPerfil(int idTipoRecursoPerfil) {
		this.idTipoRecursoPerfil = idTipoRecursoPerfil;
	}
	public String getRecurso() {
		return recurso;
	}
	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}
	public TipoPerfilUsuarioDTO getPerfil() {
		return perfil;
	}
	public void setPerfil(TipoPerfilUsuarioDTO perfil) {
		this.perfil = perfil;
	}
}
