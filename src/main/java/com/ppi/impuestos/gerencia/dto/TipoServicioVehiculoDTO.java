package com.ppi.impuestos.gerencia.dto;

public class TipoServicioVehiculoDTO {

	private int idTipoServicioVehiculo;
	private String servicio;
	
	public int getIdTipoServicioVehiculo() {
		return idTipoServicioVehiculo;
	}
	public void setIdTipoServicioVehiculo(int idTipoServicioVehiculo) {
		this.idTipoServicioVehiculo = idTipoServicioVehiculo;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
}
