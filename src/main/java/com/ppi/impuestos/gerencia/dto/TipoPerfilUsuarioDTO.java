package com.ppi.impuestos.gerencia.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TipoPerfilUsuarioDTO {

	private Integer idTipoPerfilUsuario;
	private String perfil;
	
	public int getIdTipoPerfilUsuario() {
		return idTipoPerfilUsuario;
	}
	public void setIdTipoPerfilUsuario(int idTipoPerfilUsuario) {
		this.idTipoPerfilUsuario = idTipoPerfilUsuario;
	}
	public String getPerfil() {
		return perfil;
	}
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
}
