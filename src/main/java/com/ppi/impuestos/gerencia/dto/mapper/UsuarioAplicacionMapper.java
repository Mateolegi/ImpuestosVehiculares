package com.ppi.impuestos.gerencia.dto.mapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.UsuarioAplicacion;
import com.ppi.impuestos.core.util.AES256;
import com.ppi.impuestos.gerencia.dao.PersonaNaturalDAO;
import com.ppi.impuestos.gerencia.dto.UsuarioAplicacionDTO;

public class UsuarioAplicacionMapper {
	
	private UsuarioAplicacionMapper() { }

	public static UsuarioAplicacionDTO toDTO(UsuarioAplicacion entity) {
		UsuarioAplicacionDTO dto = new UsuarioAplicacionDTO();
		dto.setIdUsuario(entity.getIdUsuario());
		dto.setNombreUsuario(entity.getNombreUsuario());
		dto.setContrasena(entity.getContrasena());
		dto.setPersonaNatural(PersonaNaturalMapper.toDTO(entity.getPersonaNatural()));
		dto.setPerfiles(TipoPerfilUsuarioMapper.toDTOList(entity.getTipoPerfilUsuarios()));
		dto.setRecursos(TipoRecursoPerfilMapper.toDTOList(entity.getTipoRecursoPerfils()));
		dto.setEstado(entity.getEstado());
		return dto;
	}
	
	public static List<UsuarioAplicacionDTO> toDTOList(List<UsuarioAplicacion> entities) {
		List<UsuarioAplicacionDTO> list = new ArrayList<>();
		for(UsuarioAplicacion entity: entities) list.add(toDTO(entity));
		return list;
	}
	
	public static UsuarioAplicacion toEntity(UsuarioAplicacionDTO dto) throws ParseException {
		UsuarioAplicacion entity = new UsuarioAplicacion();
		entity.setNombreUsuario(dto.getNombreUsuario());
		entity.setContrasena(AES256.encrypt(dto.getContrasena()));
		entity.setEstado(dto.getEstado());
		entity.setPersonaNatural(PersonaNaturalMapper.toEntity(dto.getPersonaNatural()));
		return entity;
	}
	
	public static UsuarioAplicacion update(UsuarioAplicacion entity, UsuarioAplicacionDTO dto) throws ParseException {
		if(dto.getContrasena() != null && !AES256.decrypt(entity.getContrasena()).equals(dto.getContrasena())) 
				entity.setContrasena(AES256.encrypt(dto.getContrasena()));
		if(dto.getEstado() != null) entity.setEstado(dto.getEstado());
		if(dto.getPerfiles() != null) entity.setTipoPerfilUsuarios(TipoPerfilUsuarioMapper.toEntityList(dto.getPerfiles()));
		if(dto.getRecursos() != null) entity.setTipoRecursoPerfils(TipoRecursoPerfilMapper.toEntityList(dto.getRecursos()));
		entity.setPersonaNatural(PersonaNaturalMapper
				.update(new PersonaNaturalDAO().find(dto.getPersonaNatural().getIdPersonaNatural()), dto.getPersonaNatural()));
		return entity;
	}
}
