package com.ppi.impuestos.gerencia.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import com.ppi.impuestos.core.entity.TipoServicioVehiculo;
import com.ppi.impuestos.gerencia.dto.TipoServicioVehiculoDTO;

public class TipoServicioVehiculoMapper {
	
	private TipoServicioVehiculoMapper() { }

	public static TipoServicioVehiculoDTO toDTO(TipoServicioVehiculo entity) {
		TipoServicioVehiculoDTO dto = new TipoServicioVehiculoDTO();
		dto.setIdTipoServicioVehiculo(entity.getIdServicioVehiculo());
		dto.setServicio(entity.getServicio());
		return dto;
	}
	
	public static List<TipoServicioVehiculoDTO> toDTOList(List<TipoServicioVehiculo> entities) {
		List<TipoServicioVehiculoDTO> list = new ArrayList<>();
		for(TipoServicioVehiculo entity: entities) list.add(toDTO(entity));
		return list;
	}
}
