package com.ppi.impuestos.gerencia.dto;

import java.util.List;

public class UsuarioAplicacionDTO {

	// Datos usuario
	private int idUsuario;
	private String nombreUsuario;
	private String contrasena;
	private PersonaNaturalDTO personaNatural;
	private byte estado;
	private List<TipoPerfilUsuarioDTO> perfiles;
	private List<TipoRecursoPerfilDTO> recursos;
	
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public PersonaNaturalDTO getPersonaNatural() {
		return personaNatural;
	}
	public void setPersonaNatural(PersonaNaturalDTO personaNatural) {
		this.personaNatural = personaNatural;
	}
	public Byte getEstado() {
		return estado;
	}
	public void setEstado(byte estado) {
		this.estado = estado;
	}
	public List<TipoPerfilUsuarioDTO> getPerfiles() {
		return perfiles;
	}
	public void setPerfiles(List<TipoPerfilUsuarioDTO> perfiles) {
		this.perfiles = perfiles;
	}
	public List<TipoRecursoPerfilDTO> getRecursos() {
		return recursos;
	}
	public void setRecursos(List<TipoRecursoPerfilDTO> recursos) {
		this.recursos = recursos;
	}
}
