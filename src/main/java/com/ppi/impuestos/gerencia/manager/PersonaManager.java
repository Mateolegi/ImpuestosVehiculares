package com.ppi.impuestos.gerencia.manager;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.Persona;
import com.ppi.impuestos.core.entity.PersonaNatural;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dao.PersonaDAO;
import com.ppi.impuestos.gerencia.dao.PersonaNaturalDAO;
import com.ppi.impuestos.gerencia.dto.PersonaNaturalDTO;
import com.ppi.impuestos.gerencia.dto.mapper.PersonaJuridicaMapper;
import com.ppi.impuestos.gerencia.dto.mapper.PersonaMapper;
import com.ppi.impuestos.gerencia.dto.mapper.PersonaNaturalMapper;

import javassist.NotFoundException;

public class PersonaManager implements CrudManager {

//	private static final Logger LOGGER = LogManager.getLogger(ParametroImpuestoManager.class);
	private PersonaDAO dao = new PersonaDAO();
	private Gson gson;

	public PersonaManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	@Override
	public String findAll(UriInfo uriInfo) {
		String numeroDocumento = uriInfo.getQueryParameters().getFirst("numeroDocumento");
		if (numeroDocumento != null) {
			return findByNumeroDocumento(numeroDocumento);
		}
		return gson.toJson(PersonaMapper.toDTOList(dao.findAll()));
	}

	public String findByNumeroDocumento(String numeroDocumento) {
		Persona persona = dao.findByNumeroDocumento(numeroDocumento);
		if (persona.getPersonaNatural() != null) {
			return gson.toJson(PersonaNaturalMapper.toDTO(persona.getPersonaNatural()));
		} else if (persona.getPersonaJuridica() != null) {
			return gson.toJson(PersonaJuridicaMapper.toDTO(persona.getPersonaJuridica()));
		}
		return null;
	}
	
	public String savePersonaNatural(PersonaNaturalDTO dto) throws ParseException {
		try {
			PersonaNatural personaNatural = PersonaNaturalMapper.toEntity(dto);
			personaNatural.getPersona().setFechaRegistro(new Timestamp(new Date().getTime()));
			Persona persona = new PersonaDAO().save(personaNatural.getPersona());
			personaNatural.setPersona(persona);
			return gson.toJson(PersonaNaturalMapper.toDTO(new PersonaNaturalDAO().save(personaNatural)));
		} catch (Exception e) {
			if (e.getMessage().contains("Error Code: 1062")) {
				// TODO error duplicate entry
			}
			throw new BadRequestException();
		}
	}
	
//	public String savePersonaJuridica(PersonaJuridicaDTO dto) throws ParseException {
//		PersonaJuridica personaJuridica = PersonaJuridicaMapper.toEntity(dto);
//		Persona persona = new PersonaDAO().save(personaNatural.getPersona());
//		personaJuridica.setPersona(persona);
//		return gson.toJson(PersonaJuridicaMapper.toDTO(new PersonaNaturalDAO().save(personaNatural)));
//	}

	@Override
	public String find(int id) throws NotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
