package com.ppi.impuestos.gerencia.manager;

import java.net.URI;

import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.ParametroImpuesto;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dao.ParametroImpuestoDAO;
import com.ppi.impuestos.gerencia.dto.ParametroImpuestoDTO;
import com.ppi.impuestos.gerencia.dto.mapper.ParametroImpuestoMapper;

import javassist.NotFoundException;

public class ParametroImpuestoManager implements CrudManager {
	
	private static final Logger LOGGER = LogManager.getLogger(ParametroImpuestoManager.class);
	private ParametroImpuestoDAO dao = new ParametroImpuestoDAO();
	private Gson gson;
	
	public ParametroImpuestoManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#findAll(javax.ws.rs.core.UriInfo)
	 */
	@Override
	public String findAll(UriInfo uriInfo) {
		return gson.toJson(ParametroImpuestoMapper.toDTOList(dao.findAll()));
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#find(int)
	 */
	@Override
	public String find(int id) throws NotFoundException {
		ParametroImpuesto parametro = dao.find(id);
		if(parametro == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(ParametroImpuestoMapper.toDTO(parametro));
	}

	/**
	 * Almacena un nuevo parámetro de impuesto en la base de datos. <br>
	 * Convierte el dto enviado en la solicitud a una entidad de JPA y
	 * se envía al DAO para su persistencia
	 * @param dto objeto de transferencia de Parametro de impuesto
	 * @param uriInfo información de la URI, se usa para obtener la URI de la petición
	 * @return respuesta con la entidad almacenada y un encabezado de ubicación con la 
	 * URI de la entidad
	 */
	public Response save(ParametroImpuestoDTO dto, UriInfo uriInfo) {
		try {	
			ParametroImpuesto entity = dao.save(ParametroImpuestoMapper.toEntity(dto));
			URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(entity.getIdParametroImpuesto())).build();		
			return Response.created(uri).entity(gson.toJson(ParametroImpuestoMapper.toDTO(entity))).build();
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.serverError().build();
		}
	}

	public String update (int id, ParametroImpuestoDTO dto) {
		try {
			ParametroImpuesto entity = ParametroImpuestoMapper.toEntity(dto);
			entity.setIdParametroImpuesto(id);
			return gson.toJson(ParametroImpuestoMapper.toDTO(dao.update(entity)));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServerErrorException(500);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.ppi.impuestos.core.manager.CrudManager#delete(int)
	 */
	@Override
	public Response delete (int id) {
		dao.delete(dao.find(id));
		if(dao.find(id) == null) return Response.noContent().build();
		return Response.serverError().build();
	}
}
