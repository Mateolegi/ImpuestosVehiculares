package com.ppi.impuestos.gerencia.manager;

import java.net.URI;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.TipoPerfilUsuario;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dao.PerfilUsuarioDAO;
import com.ppi.impuestos.gerencia.dto.TipoPerfilUsuarioDTO;
import com.ppi.impuestos.gerencia.dto.mapper.TipoPerfilUsuarioMapper;

public class PerfilUsuarioManager implements CrudManager {

	private static final Logger LOGGER = LogManager.getLogger(ParametroImpuestoManager.class);
	private PerfilUsuarioDAO dao = new PerfilUsuarioDAO();
	private Gson gson;
	
	public PerfilUsuarioManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	@Override
	public String findAll(UriInfo uriInfo) {
		return gson.toJson(TipoPerfilUsuarioMapper.toDTOList(dao.findAll()));
	}
	
	public String find(int id) {
		TipoPerfilUsuario perfilUsuario = dao.find(id);
		if(perfilUsuario == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(TipoPerfilUsuarioMapper.toDTO(perfilUsuario));
	}
	
	public Response save(TipoPerfilUsuarioDTO dto, UriInfo uriInfo) {
		try {	
			TipoPerfilUsuario entity = dao.save(TipoPerfilUsuarioMapper.toEntity(dto));
			URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(entity.getIdTipoPerfilUsuario())).build();		
			return Response.created(uri).entity(gson.toJson(TipoPerfilUsuarioMapper.toDTO(entity))).build();
		} catch (Exception e) {
			LOGGER.error(e);
			return Response.serverError().build();
		}
	}
	
	public String update(int id, TipoPerfilUsuarioDTO dto) {
		try {
			TipoPerfilUsuario entity = TipoPerfilUsuarioMapper.toEntity(dto);
			entity.setIdTipoPerfilUsuario(id);
			return gson.toJson(TipoPerfilUsuarioMapper.toDTO(dao.update(entity)));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServerErrorException(500);
		}
	}
	
	public Response delete(int id) {
		dao.delete(dao.find(id));
		if(dao.find(id) == null) return Response.noContent().build();
		return Response.serverError().build();
	}
}
