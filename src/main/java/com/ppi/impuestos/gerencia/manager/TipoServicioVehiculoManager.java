package com.ppi.impuestos.gerencia.manager;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.dao.TipoServicioVehiculoDAO;
import com.ppi.impuestos.core.entity.TipoServicioVehiculo;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dto.mapper.TipoServicioVehiculoMapper;

import javassist.NotFoundException;

public class TipoServicioVehiculoManager implements CrudManager {

	private TipoServicioVehiculoDAO dao = new TipoServicioVehiculoDAO();
	private Gson gson;
	
	public TipoServicioVehiculoManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	@Override
	public String findAll(UriInfo uriInfo) {
		return gson.toJson(TipoServicioVehiculoMapper.toDTOList(dao.findAll()));
	}

	@Override
	public String find(int id) throws NotFoundException {
		TipoServicioVehiculo servicioVehiculo = dao.find(id);
		if(servicioVehiculo == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(TipoServicioVehiculoMapper.toDTO(servicioVehiculo));
	}

	@Override
	public Response delete(int id) {
		dao.delete(dao.find(id));
		if(dao.find(id) == null) return Response.noContent().build();
		return Response.serverError().build();
	}

}
