package com.ppi.impuestos.gerencia.manager;

import java.net.URI;

import javax.persistence.EntityNotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.TipoPerfilUsuario;
import com.ppi.impuestos.core.entity.TipoRecursoPerfil;
import com.ppi.impuestos.gerencia.dao.PerfilUsuarioDAO;
import com.ppi.impuestos.gerencia.dao.RecursoPerfilDAO;
import com.ppi.impuestos.gerencia.dto.TipoRecursoPerfilDTO;
import com.ppi.impuestos.gerencia.dto.mapper.TipoPerfilUsuarioMapper;
import com.ppi.impuestos.gerencia.dto.mapper.TipoRecursoPerfilMapper;

public class RecursoPerfilManager {

	private RecursoPerfilDAO dao = new RecursoPerfilDAO();
	private Gson gson;
	
	public RecursoPerfilManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	public String findAll(int perfilId) {
		return gson.toJson(TipoRecursoPerfilMapper.toDTOList(dao.findAll(checkPerfil(perfilId))));
	}

	public String find(int perfilId, int id) {
		return gson.toJson(TipoRecursoPerfilMapper.toDTO(dao.find(checkPerfil(perfilId), id)));
	}

	public Response save(int perfilId, TipoRecursoPerfilDTO dto, UriInfo uriInfo) {
		TipoPerfilUsuario perfil = checkPerfil(perfilId);
		dto.setPerfil(TipoPerfilUsuarioMapper.toDTO(perfil));
		TipoRecursoPerfil nuevoRecurso = dao.save(TipoRecursoPerfilMapper.toEntity(dto));
		URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(nuevoRecurso.getIdTipoRecursoPerfil())).build();
		return Response.created(uri).entity(gson.toJson(nuevoRecurso)).build();
	}

	public String update(int perfilId, int id, TipoRecursoPerfilDTO dto) {
		TipoPerfilUsuario perfil = checkPerfil(perfilId);
		dto.setIdTipoRecursoPerfil(id);
		dto.setPerfil(TipoPerfilUsuarioMapper.toDTO(perfil));
		TipoRecursoPerfil entity = dao.update(TipoRecursoPerfilMapper.toEntity(dto));
		return gson.toJson(TipoRecursoPerfilMapper.toDTO(entity));
	}

	public Response delete(int perfilId, int id) {
		checkPerfil(perfilId);
		dao.delete(dao.find(id));
		return Response.noContent().build();
	}

	private TipoPerfilUsuario checkPerfil(int perfilId) {
		PerfilUsuarioDAO perfilDao = new PerfilUsuarioDAO();
		TipoPerfilUsuario perfil = perfilDao.find(perfilId);
		if (perfil == null)
			throw new EntityNotFoundException("No se encontró un perfil con el id " + perfilId);
		return perfil;
	}
}
