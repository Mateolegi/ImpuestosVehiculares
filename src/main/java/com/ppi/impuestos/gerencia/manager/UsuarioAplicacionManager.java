package com.ppi.impuestos.gerencia.manager;

import java.net.URI;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.EntityExistsException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.entity.UsuarioAplicacion;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dao.PersonaDAO;
import com.ppi.impuestos.gerencia.dao.PersonaNaturalDAO;
import com.ppi.impuestos.gerencia.dao.UsuarioAplicacionDAO;
import com.ppi.impuestos.gerencia.dto.UsuarioAplicacionDTO;
import com.ppi.impuestos.gerencia.dto.mapper.UsuarioAplicacionMapper;

public class UsuarioAplicacionManager implements CrudManager {

	private UsuarioAplicacionDAO dao = new UsuarioAplicacionDAO();
	private Gson gson;
	private static final Logger LOGGER = LogManager.getLogger(UsuarioAplicacionManager.class);
	
	public UsuarioAplicacionManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}
	
	public String findAll(UriInfo uriInfo) {
		try {
			return gson.toJson(UsuarioAplicacionMapper.toDTOList(dao.findAll()));
		} catch (Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}
	
	public String find(int id) {
		try {
			UsuarioAplicacion usuarioAplicacion = dao.find(id);
			if (usuarioAplicacion == null)
				throw new NotFoundException("No existe un usuario identificado con el id " + id);
			return gson.toJson(UsuarioAplicacionMapper.toDTO(usuarioAplicacion));
		} catch (Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}
	
	public Response save(UsuarioAplicacionDTO dto, UriInfo uriInfo) throws Exception {
		try {
			UsuarioAplicacion usuarioAplicacion = UsuarioAplicacionMapper.toEntity(dto);
			usuarioAplicacion.getPersonaNatural().getPersona().setFechaRegistro(new Timestamp(new Date().getTime()));
			usuarioAplicacion.getPersonaNatural().setPersona(new PersonaDAO()
					.save(usuarioAplicacion.getPersonaNatural().getPersona()));
			usuarioAplicacion.setPersonaNatural(new PersonaNaturalDAO()
					.save(usuarioAplicacion.getPersonaNatural()));
			usuarioAplicacion = dao.save(usuarioAplicacion);
			URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(usuarioAplicacion.getIdUsuario())).build();
			return Response.created(uri).entity(gson.toJson(usuarioAplicacion)).build();
		} catch (Exception e) {
			if (e.getMessage().contains("Duplicate entry")) {
				LOGGER.warn("La entidad existe", e);
				throw new EntityExistsException("La entidad ya existe.");
			} else {
				LOGGER.error(e);
				throw e;
			}
		}
	}
	
	public String update(int id, UsuarioAplicacionDTO dto) throws Exception {
		try {
			return gson.toJson(dao.update(UsuarioAplicacionMapper.update(dao.find(id), dto)));
		} catch (Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}
	
	public Response delete(int id) {
		try {
			dao.delete(dao.find(id));
			return Response.noContent().build();
		} catch (Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}
}
