package com.ppi.impuestos.gerencia.manager;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ppi.impuestos.core.dao.TipoVehiculoDAO;
import com.ppi.impuestos.core.entity.TipoVehiculo;
import com.ppi.impuestos.core.manager.CrudManager;
import com.ppi.impuestos.gerencia.dto.mapper.TipoVehiculoMapper;

import javassist.NotFoundException;

public class TipoVehiculoManager implements CrudManager {

	private TipoVehiculoDAO dao = new TipoVehiculoDAO();
	private Gson gson;

	public TipoVehiculoManager() {
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting().serializeNulls();
		gson = builder.create();
	}

	@Override
	public String findAll(UriInfo uriInfo) {
		return gson.toJson(TipoVehiculoMapper.toDTOList(dao.findAll()));
	}

	@Override
	public String find(int id) throws NotFoundException {
		TipoVehiculo tipoVehiculo = dao.find(id);
		if(tipoVehiculo == null) 
			throw new NotFoundException ("No se encuenta el id " + id);
		return gson.toJson(TipoVehiculoMapper.toDTO(tipoVehiculo));
	}

	@Override
	public Response delete(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}
