package com.ppi.impuestos.gerencia.dao;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.UsuarioAplicacion;

public class UsuarioAplicacionDAO extends CrudDAO<UsuarioAplicacion> {
	public UsuarioAplicacionDAO() {
		super(UsuarioAplicacion.class);
	}

	/**
	 * Obtiene un usuario de la aplicación por su nombre de usuario
	 * @param username nombre de usuario
	 * @return usuario de la aplicación
	 */
	public UsuarioAplicacion findByUsername(String username) {
		return entityManager.createNamedQuery("UsuarioAplicacion.findByUsername", UsuarioAplicacion.class)
				.setParameter("username", username).getSingleResult();
	}

	/**
	 * Obtiene un usuario de la aplicación por su correo electrónico
	 * @param email
	 * @return
	 */
	public UsuarioAplicacion findByEmail(String email) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT U.* FROM USUARIO_APLICACION U ")
		   .append("INNER JOIN PERSONA_NATURAL PN ON U.ID_PERSONA_NATURAL = PN.ID_PERSONA_NATURAL ")
		   .append("INNER JOIN PERSONA P ON PN.ID_PERSONA = P.ID_PERSONA WHERE P.EMAIL = :email");
		return (UsuarioAplicacion) entityManager.createNativeQuery(sql.toString(), UsuarioAplicacion.class)
				.setParameter("email", email).getSingleResult();
	}
}
