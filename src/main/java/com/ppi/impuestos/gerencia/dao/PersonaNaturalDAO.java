package com.ppi.impuestos.gerencia.dao;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.PersonaNatural;

public class PersonaNaturalDAO extends CrudDAO<PersonaNatural> {
	public PersonaNaturalDAO() {
		super(PersonaNatural.class);
	}
}
