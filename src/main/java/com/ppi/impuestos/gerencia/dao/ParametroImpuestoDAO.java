
package com.ppi.impuestos.gerencia.dao;

import java.util.List;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.ParametroImpuesto;
import com.ppi.impuestos.core.entity.TipoServicioVehiculo;
import com.ppi.impuestos.core.entity.TipoVehiculo;

public class ParametroImpuestoDAO extends CrudDAO<ParametroImpuesto> {
	public ParametroImpuestoDAO() {
		super(ParametroImpuesto.class);
	}
	
	public List<ParametroImpuesto> findByServicioTipo(TipoServicioVehiculo servicio, TipoVehiculo tipo) {
		return entityManager.createNamedQuery("ParametroImpuesto.findByServicioTipo", ParametroImpuesto.class)
				.setParameter("tipo", tipo)
				.setParameter("servicio", servicio)
				.getResultList();
	}
}
