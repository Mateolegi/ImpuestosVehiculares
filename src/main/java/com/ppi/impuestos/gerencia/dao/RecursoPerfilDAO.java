package com.ppi.impuestos.gerencia.dao;

import java.util.List;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.TipoPerfilUsuario;
import com.ppi.impuestos.core.entity.TipoRecursoPerfil;

public class RecursoPerfilDAO extends CrudDAO<TipoRecursoPerfil> {
	public RecursoPerfilDAO() {
		super(TipoRecursoPerfil.class);
	}

	public List<TipoRecursoPerfil> findAll(TipoPerfilUsuario perfil) {
		return entityManager
				.createQuery("FROM TipoRecursoPerfil t WHERE t.tipoPerfilUsuario = :perfil ORDER BY t.idTipoRecursoPerfil", 
				TipoRecursoPerfil.class).setParameter("perfil", perfil).getResultList();
	}
	
	public TipoRecursoPerfil find(TipoPerfilUsuario perfil, int id) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("FROM TipoRecursoPerfil t WHERE t.tipoPerfilUsuario = :perfil and t.idTipoRecursoPerfil = :recurso ");
		jpql.append("ORDER BY t.idTipoRecursoPerfil");
		return entityManager.createQuery(jpql.toString(), TipoRecursoPerfil.class)
				.setParameter("perfil", perfil)
				.setParameter("recurso", id)
				.getSingleResult();
	}
}
