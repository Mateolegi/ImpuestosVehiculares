package com.ppi.impuestos.gerencia.dao;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.TipoPerfilUsuario;

public class PerfilUsuarioDAO extends CrudDAO<TipoPerfilUsuario> {
	public PerfilUsuarioDAO() {
		super(TipoPerfilUsuario.class);
	}
}
