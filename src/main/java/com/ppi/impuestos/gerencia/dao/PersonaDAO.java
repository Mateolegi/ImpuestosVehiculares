package com.ppi.impuestos.gerencia.dao;

import com.ppi.impuestos.core.dao.CrudDAO;
import com.ppi.impuestos.core.entity.Persona;

public class PersonaDAO extends CrudDAO<Persona> {
	public PersonaDAO() {
		super(Persona.class);
	}
	
	public Persona findByNumeroDocumento(String numeroDocumento) {
		return entityManager.createNamedQuery("Persona.findByNumeroDocumento", Persona.class)
				.setParameter("numeroDocumento", numeroDocumento)
				.getSingleResult();
	}
}
