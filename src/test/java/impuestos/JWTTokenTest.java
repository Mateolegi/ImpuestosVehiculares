package impuestos;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ppi.impuestos.core.manager.LoginManager;
import com.ppi.impuestos.core.util.KeyUtils;

import io.jsonwebtoken.Jwts;

public class JWTTokenTest {

	@Test
	public void tokenGenerationTest() {
		LoginManager manager = new LoginManager();
		String jwt = manager.generateToken(1, "Mateo", "");
		assertEquals("El token no se generó correctamente", "Mateo", 
				Jwts.parser().setSigningKey(KeyUtils.KEY)
				.parseClaimsJws(jwt).getBody().getSubject());
	}
}
