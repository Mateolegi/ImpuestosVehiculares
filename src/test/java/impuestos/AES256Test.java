package impuestos;

import static org.junit.Assert.*;

import org.junit.Test;

import com.ppi.impuestos.core.util.AES256;

public class AES256Test {

	@Test
	public void testEncrypt() {
		String originalText = "contrasena";
		assertNotNull("El cifrado retorno nulo", AES256.encrypt(originalText));
	}

	@Test
	public void testDecrypt() {
		String encryptedText = "lGuFw3goUkytrsFx3nTd3FTW9QF1Y06K/isDaivYPxGzBqibA3jv47I2+n9qC7K5ZgkyoA==";
		String originalText = "contrasena";
		assertEquals("El descifrado ha retornado un text diferente", originalText, AES256.decrypt(encryptedText));
	}
}
